<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kecamatan_m extends CI_Model{

	// ambil data dari tabel kecamatan
	public function getDetailKec($id=null) {
		$this->db->from('kecamatan');
		if($id != null){
			$this->db->where('id_kec', $id);
		}
		$query = $this->db->get();
		return $query;
	}

	// edit data
	public function edit($post)
	{
		// $params['luas_kec'] = $post['luas_kec'];
		$params['penduduk_kec'] = $post['penduduk_kec'];
		$this->db->where('id_kec', $post['id_kec']);
		$this->db->update('kecamatan', $params);
	}

	// ambil gambar kecamatan
	public function getGbrKec($id=null)
    {
        $sesi = $this->session->userdata("username");
        $this->db->select('image_kec');
        $this->db->from('kecamatan');
        $this->db->where('username = ', $sesi);
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            return $query->row()->image_kec;
        }
        return false;
    }

	// `d`.`username` AS `username` (`d`.`username` = `a`.`username`)
	// public function delete($id)
	// {
	// 	$this->db->where('id_akun', $id);
	// 	$this->db->delete('akun');
	// }

	// public function getKec($id=null)
	// {
	// 	$this->db->from('kecamatan');
	// 	if($id != null){
	// 		$this->db->where('id_kec', $id);
	// 	}
	// 	$query = $this->db->get();
	// 	return $query;
	// }
}