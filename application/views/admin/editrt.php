<section class="content">
  <div class="box box-primary">

    <!-- content header -->
    <div class="box-header with-border">
      <h2 class="box-title"><big>Edit <?=$row->nama_rt?> - <?=$row->nama_kel?> - <?=$row->nama_kec?></small></big></h2>
      <div class="pull-right">
          <a href="<?=site_url('admin/rt')?>" class="btn btn-warning btn-flat">
            <i class="fa fa-arrow-left"></i>
          </a>
      </div>
    </div>
    <!-- end content header -->

    <!-- isi content -->
    <div class="box-body">
      <div class="row">
      <div class="col-md-4">
        <form action="" method="post">
          <!-- <div class="form-group <?=form_error('luas_rt') ? 'has-error' : null?>">
            <label>Luas Wilayah<small> (km2)</small></label>
              <input type="hidden" name="id_rt" value="<?=$row->id_rt?>">
              <input type="text" name="luas_rt"  value="<?=$this->input->post('luas_rt') ??  $row->luas_rt?>" class="form-control">
            <?=form_error('luas_rt')?>
          </div> -->
          <div class="form-group <?=form_error('penduduk_rt') ? 'has-error' : null?>">
            <label>Jumlah Penduduk</label>
              <input type="hidden" name="id_rt" value="<?=$row->id_rt?>">
              <input type="text" name="penduduk_rt" value="<?=$this->input->post('penduduk_rt') ??  $row->penduduk_rt?>" class="form-control">
            <?=form_error('penduduk_rt')?>
          </div>
          <div class="form-group">
            <button class="btn btn-success btn-flat" type="submit">
              <i class="fa fa-paper-plane"></i> Simpan
            </button>
            <button class="btn btn-flat" type="reset">Reset</button>
          </div>
        </form>
      </div>
    </div>
    <!-- end isi content -->

  </div>
</section>