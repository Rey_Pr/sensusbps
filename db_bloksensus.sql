-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 28, 2020 at 01:48 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_bloksensus`
--

-- --------------------------------------------------------

--
-- Table structure for table `akun`
--

CREATE TABLE `akun` (
  `id_akun` int(20) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `status` char(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `akun`
--

INSERT INTO `akun` (`id_akun`, `username`, `password`, `nama`, `status`) VALUES
(1, 'admin', '827ccb0eea8a706c4c34a16891f84e7b', 'Master', '1'),
(2, 'karangpilang', '827ccb0eea8a706c4c34a16891f84e7b', 'Karang Pilang', '2'),
(3, 'jambangan', '827ccb0eea8a706c4c34a16891f84e7b', 'Jambangan', '2'),
(4, 'gayungan', '827ccb0eea8a706c4c34a16891f84e7b', 'Gayungan', '2'),
(8, 'wonocolo', '827ccb0eea8a706c4c34a16891f84e7b', 'Wonocolo', '2'),
(9, 'tenggilismejoyo', '827ccb0eea8a706c4c34a16891f84e7b', 'Tenggilis Mejoyo', '2'),
(10, 'gununganyar', '827ccb0eea8a706c4c34a16891f84e7b', 'Gunung Anyar', '2'),
(11, 'rungkut', '827ccb0eea8a706c4c34a16891f84e7b', 'Rungkut', '2'),
(12, 'sukolilo', '827ccb0eea8a706c4c34a16891f84e7b', 'Sukolilo', '2'),
(13, 'mulyorejo', '827ccb0eea8a706c4c34a16891f84e7b', 'Mulyorejo', '2'),
(14, 'gubeng', '827ccb0eea8a706c4c34a16891f84e7b', 'Gubeng', '2'),
(15, 'wonokromo', '827ccb0eea8a706c4c34a16891f84e7b', 'Wonokromo', '2'),
(16, 'dukuhpakis', '827ccb0eea8a706c4c34a16891f84e7b', 'Dukuh Pakis', '2'),
(17, 'wiyung', '827ccb0eea8a706c4c34a16891f84e7b', 'Wiyung', '2'),
(18, 'lakarsantri', '827ccb0eea8a706c4c34a16891f84e7b', 'Lakarsantri', '2'),
(19, 'sambikerep', '827ccb0eea8a706c4c34a16891f84e7b', 'Sambikerep', '2'),
(20, 'tandes', '827ccb0eea8a706c4c34a16891f84e7b', 'Tandes', '2'),
(21, 'sukomanunggal', '827ccb0eea8a706c4c34a16891f84e7b', 'Suko Manunggal', '2'),
(22, 'sawahan', '827ccb0eea8a706c4c34a16891f84e7b', 'Sawahan', '2'),
(23, 'tegalsari', '827ccb0eea8a706c4c34a16891f84e7b', 'Tegalsari', '2'),
(24, 'genteng', '827ccb0eea8a706c4c34a16891f84e7b', 'Genteng', '2'),
(25, 'tambaksari', '827ccb0eea8a706c4c34a16891f84e7b', 'Tambaksari', '2'),
(26, 'kenjeran', '827ccb0eea8a706c4c34a16891f84e7b', 'Kenjeran', '2'),
(27, 'bulak', '827ccb0eea8a706c4c34a16891f84e7b', 'Bulak', '2'),
(28, 'simokerto', '827ccb0eea8a706c4c34a16891f84e7b', 'Simokerto', '2'),
(29, 'semampir', '827ccb0eea8a706c4c34a16891f84e7b', 'Semampir', '2'),
(30, 'pabeancantian', '827ccb0eea8a706c4c34a16891f84e7b', 'Pabean Cantian', '2'),
(31, 'bubutan', '827ccb0eea8a706c4c34a16891f84e7b', 'Bubutan', '2'),
(32, 'krembangan', '827ccb0eea8a706c4c34a16891f84e7b', 'Krembangan', '2'),
(33, 'asemrowo', '827ccb0eea8a706c4c34a16891f84e7b', 'Asemrowo', '2'),
(34, 'benowo', '827ccb0eea8a706c4c34a16891f84e7b', 'Benowo', '2'),
(35, 'pakal', '827ccb0eea8a706c4c34a16891f84e7b', 'Pakal', '2');

-- --------------------------------------------------------

--
-- Table structure for table `kecamatan`
--

CREATE TABLE `kecamatan` (
  `id_kec` varchar(255) NOT NULL,
  `nama_kec` varchar(255) NOT NULL,
  `luas_kec` decimal(6,2) DEFAULT NULL,
  `penduduk_kec` int(7) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `image_kec` varchar(255) DEFAULT NULL,
  `jumlah_kel` int(2) NOT NULL,
  `jumlah_rt` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kecamatan`
--

INSERT INTO `kecamatan` (`id_kec`, `nama_kec`, `luas_kec`, `penduduk_kec`, `username`, `image_kec`, `jumlah_kel`, `jumlah_rt`) VALUES
('010', 'Kecamatan Karangpilang', '9.24', 75658, 'karangpilang', 'karangpilang.png', 4, NULL),
('020', 'Kecamatan Jambangan', '4.19', 52950, 'jambangan', 'jambangan.png', 4, NULL),
('030', 'Kecamatan Gayungan', '6.16', 47064, 'gayungan', 'gayungan.png', 4, NULL),
('040', 'Kecamatan Wonocolo', '6.11', 83749, 'wonocolo', 'wonocolo.jpg', 5, NULL),
('050', 'Kecamatan Tenggilis Mejoyo', '5.47', 59555, 'tenggilismejoyo', 'tenggilismejoyo.jpg', 4, NULL),
('060', 'Kecamatan Gunung Anyar', '9.20', 58714, 'gununganyar', 'gununganyar.jpg', 4, NULL),
('070', 'Kecamatan Rungkut', '21.02', 117591, 'rungkut', 'rungkut.jpg', 6, NULL),
('080', 'Kecamatan Sukolilo', '23.66', 114309, 'sukolilo', 'sukolilo.jpg', 7, NULL),
('090', 'Kecamatan Mulyorejo', '11.94', 90734, 'mulyorejo', 'mulyorejo.jpg', 6, NULL),
('100', 'Kecamatan Gubeng', '7.48', 132123, 'gubeng', 'gubeng.jpg', 6, NULL),
('110', 'Kecamatan Wonokromo', '6.70', 167720, 'wonokromo', 'wonokromo.jpg', 6, NULL),
('120', 'Kecamatan Dukuh Pakis', '10.02', 62520, 'dukuhpakis', 'dukuhpakis.jpg', 4, NULL),
('130', 'Kecamatan Wiyung', '11.52', 72720, 'wiyung', 'wiyung.jpg', 4, NULL),
('140', 'Kecamatan Lakarsantri', '17.73', 59930, 'lakarsantri', 'lakarsantri.jpg', 6, NULL),
('141', 'Kecamatan Sambikerep', '18.93', 60924, 'sambikerep', 'sambikerep.jpg', 4, NULL),
('150', 'Kecamatan Tandes', '9.77', 98297, 'tandes', 'tandes.jpg', 6, NULL),
('160', 'Kecamatan Suko Manunggal', '11.20', 105917, 'sukomanunggal', 'sukomanunggal.jpg', 6, NULL),
('170', 'Kecamatan Sawahan', '7.64', 213760, 'sawahan', 'sawahan.jpg', 6, NULL),
('180', 'Kecamatan Tegalsari', '4.29', 117492, 'tegalsari', 'tegalsari.jpg', 5, NULL),
('190', 'Kecamatan Genteng', '3.41', 61934, 'genteng', 'genteng.jpg', 5, NULL),
('200', 'Kecamatan Tambaksari', '9.10', 234473, 'tambaksari', 'tambaksari.jpg', 8, NULL),
('210', 'Kecamatan Kenjeran', '7.72', 172174, 'kenjeran', 'kenjeran.jpg', 4, NULL),
('211', 'Kecamatan Bulak', '5.65', 44168, 'bulak', 'bulak.jpg', 4, NULL),
('220', 'Kecamatan Simokerto', '2.67', 102764, 'simokerto', 'simokerto.jpg', 5, NULL),
('230', 'Kecamatan Semampir', '6.14', 202029, 'semampir', 'semampir.jpg', 5, NULL),
('240', 'Kecamatan Pabean Cantian', '4.32', 89881, 'pabeancantian', 'pabeancantian.jpg', 5, NULL),
('250', 'Kecamatan Bubutan', '3.76', 106399, 'bubutan', 'bubutan.jpg', 5, NULL),
('260', 'Kecamatan Krembangan', '6.61', 124419, 'krembangan', 'krembangan.jpg', 5, NULL),
('270', 'Kecamatan Asemrowo', '13.93', 49606, 'asemrowo', 'asemrowo.jpg', 3, NULL),
('280', 'Kecamatan Benowo', '23.76', 66062, 'benowo', 'benowo.jpg', 4, NULL),
('281', 'Kecamatan Pakal', '17.59', 56453, 'pakal', 'pakal.jpg', 4, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kelurahan`
--

CREATE TABLE `kelurahan` (
  `id_kel` varchar(255) NOT NULL,
  `id_kec` varchar(255) DEFAULT NULL,
  `nama_kel` varchar(255) NOT NULL,
  `luas_kel` decimal(6,2) DEFAULT NULL,
  `penduduk_kel` int(7) NOT NULL,
  `jumlah_rt` int(2) DEFAULT NULL,
  `image_kel` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelurahan`
--

INSERT INTO `kelurahan` (`id_kel`, `id_kec`, `nama_kel`, `luas_kel`, `penduduk_kel`, `jumlah_rt`, `image_kel`) VALUES
('010-001', '010', 'Kelurahan Warugunung', '3.86', 9198, NULL, '3578010001.jpg'),
('010-002', '010', 'Kelurahan Karang Pilang', '1.44', 9569, NULL, '3578010002.jpg'),
('010-003', '010', 'Kelurahan Kebraon', '2.08', 29482, NULL, '3578010003.jpg'),
('010-004', '010', 'Kelurahan Kedurus', '1.86', 27409, NULL, '3578010004.jpg'),
('020-001', '020', 'Kelurahan Pagesangan', '1.11', 13932, NULL, '3578020001.jpg'),
('020-002', '020', 'Kelurahan Kebonsari', '0.85', 10390, NULL, '3578020002.jpg'),
('020-003', '020', 'Kelurahan Jambangan', '0.73', 10300, NULL, '3578020032.jpg'),
('020-004', '020', 'Kelurahan Karah', '1.50', 18328, NULL, '3578020004.jpg'),
('030-001', '030', 'Kelurahan Dukuh Menanggal', '1.06', 8947, NULL, '3578030001.jpg'),
('030-002', '030', 'Kelurahan Menanggal', '0.66', 12220, NULL, '3578030002.jpg'),
('030-003', '030', 'Kelurahan Gayungan', '1.47', 12220, NULL, '3578030003.jpg'),
('030-004', '030', 'Kelurahan Ketintang', '2.97', 16108, NULL, '3578030004.jpg'),
('040-001', '040', 'Kelurahan Siwalankerto', '1.98', 17683, NULL, '3578040001.jpg'),
('040-002', '040', 'Kelurahan Jemur Wonosari', '1.64', 22771, NULL, '3578040002.jpg'),
('040-003', '040', 'Kelurahan Margorejo', '0.65', 11568, NULL, '3578040003.jpg'),
('040-004', '040', 'Kelurahan Bendul Merisi', '0.77', 17678, NULL, '3578040004.jpg'),
('040-005', '040', 'Kelurahan Sidosermo', '1.07', 14049, NULL, '3578040005.jpg'),
('050-001', '050', 'Kelurahan Kutisari', '1.96', 19290, NULL, '3578050001.jpg'),
('050-002', '050', 'Kelurahan Kendangsari', '1.31', 15464, NULL, '3578050002.jpg'),
('050-003', '050', 'Kelurahan Tenggilis Mejoyo', '0.94', 11340, NULL, '3578050003.jpg'),
('050-004', '050', 'Kelurahan Panjang Jiwo', '1.26', 13461, NULL, '3578050004.jpg'),
('060-001', '060', 'Kelurahan Rungkut Menanggal', '0.92', 13879, NULL, '3578060001.jpg'),
('060-002', '060', 'Kelurahan Rungkut Tengah', '0.93', 13521, NULL, '3578060002.jpg'),
('060-003', '060', 'Kelurahan Gunung Anyar', '2.94', 21376, NULL, '3578060003.jpg'),
('060-004', '060', 'Kelurahan Gunung Anyar Tambak', '4.41', 9938, NULL, '3578060004.jpg'),
('070-001', '070', 'Kelurahan Rungkut Kidul', '1.37', 13802, NULL, '3578070001.jpg'),
('070-002', '070', 'Kelurahan Medokan Ayu', '7.23', 26802, NULL, '3578070002.jpg'),
('070-003', '070', 'Kelurahan Wonorejo', '6.48', 17424, NULL, '3578070003.jpg'),
('070-004', '070', 'Kelurahan Penjaringan Sari', '1.81', 19367, NULL, '3578070004.jpg'),
('070-005', '070', 'Kelurahan Kedung Baruk', '1.55', 16537, NULL, '3578070005.jpg'),
('070-006', '070', 'Kelurahan Kali Rungkut', '2.58', 23659, NULL, '3578070006.jpg'),
('080-001', '080', 'Kelurahan Nginden Jangkungan', '1.14', 15907, NULL, '3578080001.jpg'),
('080-002', '080', 'Kelurahan Semolowaru', '1.67', 19683, NULL, '3578080002.jpg'),
('080-003', '080', 'Kelurahan Medokan Semampir', '1.87', 19331, NULL, '3578080003.jpg'),
('080-004', '080', 'Kelurahan Keputih', '14.40', 17425, NULL, '3578080004.jpg'),
('080-005', '080', 'Kelurahan Gebang Putih', '1.33', 7614, NULL, '3578080005.jpg'),
('080-006', '080', 'Kelurahan Klampis Ngasem', '1.68', 17805, NULL, '3578080006.jpg'),
('080-007', '080', 'Kelurahan Menur Pumpungan', '1.57', 16544, NULL, '3578080007.jpg'),
('090-001', '090', 'Kelurahan Manyar Sabrangan', '1.13', 17867, NULL, '3578090001.jpg'),
('090-002', '090', 'Kelurahan Mulyorejo', '3.01', 18622, NULL, '3578090002.jpg'),
('090-003', '090', 'Kelurahan Kejawan Putih Tambak', '2.21', 7406, NULL, '3578090003.jpg'),
('090-004', '090', 'Kelurahan Kalisari', '2.13', 15887, NULL, '3578090004.jpg'),
('090-005', '090', 'Kelurahan Dukuh Sutorejo', '2.14', 16791, NULL, '3578090005.jpg'),
('090-006', '090', 'Kelurahan Kalijudan', '1.32', 14161, NULL, '3578090006.jpg'),
('100-001', '100', 'Kelurahan Barata Jaya', '0.76', 16772, NULL, '3578100001.jpg'),
('100-002', '100', 'Kelurahan Pucang Sewu', '0.94', 14926, NULL, '3578100002.jpg'),
('100-003', '100', 'Kelurahan Kertajaya', '1.30', 16772, NULL, '3578100003.jpg'),
('100-004', '100', 'Kelurahan Gubeng', '1.10', 15134, NULL, '3578100004.jpg'),
('100-005', '100', 'Kelurahan Airlangga', '1.62', 20915, NULL, '3578100005.jpg'),
('100-006', '100', 'Kelurahan Mojo', '1.76', 47604, NULL, '3578100006.jpg'),
('110-001', '110', 'Kelurahan Sawunggaling', '1.50', 28714, NULL, '3578110001.jpg'),
('110-002', '110', 'Kelurahan Wonokromo', '1.00', 42853, NULL, '3578110002.jpg'),
('110-003', '110', 'Kelurahan Jagir', '1.03', 22277, NULL, '3578110003.jpg'),
('110-004', '110', 'Kelurahan Ngagel Rejo', '1.36', 44909, NULL, '3578110004.jpg'),
('110-005', '110', 'Kelurahan Ngagel', '0.86', 12839, NULL, '3578110005.jpg'),
('110-006', '110', 'Kelurahan Darmo', '0.95', 16128, NULL, '3578110006.jpg'),
('120-001', '120', 'Kelurahan Gunung Sari', '1.63', 15014, NULL, '3578120001.jpg'),
('120-002', '120', 'Kelurahan Dukuh Pakis', '3.07', 15431, NULL, '3578120002.jpg'),
('120-003', '120', 'Kelurahan Pradah Kali Kendal', '3.96', 15882, NULL, '3578120003.jpg'),
('120-004', '120', 'Kelurahan Dukuh Kupang', '1.36', 16193, NULL, '3578120004.jpg'),
('130-001', '130', 'Kelurahan Balas Klumprik', '2.01', 13617, NULL, '3578130001.jpg'),
('130-002', '130', 'Kelurahan Babatan', '4.40', 28958, NULL, '3578130002.jpg'),
('130-003', '130', 'Kelurahan Wiyung', '3.55', 18679, NULL, '3578130003.jpg'),
('130-004', '130', 'Kelurahan Jajar Tunggal', '1.56', 11466, NULL, '3578130004.jpg'),
('140-001', '140', 'Kelurahan Bangkingan', '2.76', 8884, NULL, '3578140001.jpg'),
('140-002', '140', 'Kelurahan Sumur Welut', '2.56', 5305, NULL, '3578140002.jpg'),
('140-003', '140', 'Kelurahan Lidah Wetan', '2.78', 11624, NULL, '3578140003.jpg'),
('140-004', '140', 'Kelurahan Lidah Kulon', '3.85', 17148, NULL, '3578140004.jpg'),
('140-005', '140', 'Kelurahan Jeruk', '2.70', 8605, NULL, '3578140005.jpg'),
('140-006', '140', 'Kelurahan Lakarsantri', '3.08', 8364, NULL, '3578140006.jpg'),
('141-001', '141', 'Kelurahan Made', '4.47', 8519, NULL, '3578141001.jpg'),
('141-002', '141', 'Kelurahan Bringin', '4.11', 5369, NULL, '3578141002.jpg'),
('141-003', '141', 'Kelurahan Sambikerep', '4.50', 18261, NULL, '3578141003.jpg'),
('141-004', '141', 'Kelurahan Lontar', '5.85', 28775, NULL, '3578141004.jpg'),
('150-001', '150', 'Kelurahan Tandes', '1.07', 9900, NULL, '3578150001.jpg'),
('150-002', '150', 'Kelurahan Karangpoh', '1.55', 16439, NULL, '3578150002.jpg'),
('150-003', '150', 'Kelurahan Balongsari', '1.25', 12657, NULL, '3578150003.jpg'),
('150-004', '150', 'Kelurahan Manukan Wetan', '2.88', 9150, NULL, '3578150004.jpg'),
('150-005', '150', 'Kelurahan Manukan Kulon', '2.00', 37384, NULL, '3578150005.jpg'),
('150-006', '150', 'Kelurahan Banjarsugihan', '1.02', 12767, NULL, '3578150006.jpg'),
('160-001', '160', 'Kelurahan Putat Gede', '1.16', 7148, NULL, '3578160001.jpg'),
('160-002', '160', 'Kelurahan Sonokawijenan', '1.13', 8480, NULL, '3578160002.jpg'),
('160-003', '160', 'Kelurahan Simo Mulyo', '2.60', 24171, NULL, '3578160003.jpg'),
('160-004', '160', 'Kelurahan Sukomanunggal', '2.30', 11417, NULL, '3578160004.jpg'),
('160-005', '160', 'Kelurahan Tanjung Sari', '2.01', 12453, NULL, '3578160005.jpg'),
('160-006', '160', 'Kelurahan Simo Mulyo Baru', '2.00', 42248, NULL, '3578160006.jpg'),
('170-001', '170', 'Kelurahan Pakis', '2.47', 38630, NULL, '3578170001.jpg'),
('170-002', '170', 'Kelurahan Putat Jaya', '1.36', 48311, NULL, '3578170002.jpg'),
('170-003', '170', 'Kelurahan Banyu Urip', '0.96', 42061, NULL, '3578170003.jpg'),
('170-004', '170', 'Kelurahan Kupang Krajan', '0.60', 25376, NULL, '3578170004.jpg'),
('170-005', '170', 'Kelurahan Petemon', '1.35', 38933, NULL, '3578170005.jpg'),
('170-006', '170', 'Kelurahan Sawahan', '0.90', 20449, NULL, '3578170006.jpg'),
('180-001', '180', 'Kelurahan Keputran', '0.96', 20658, NULL, '3578180001.jpg'),
('180-002', '180', 'Kelurahan Dr. Sutomo', '1.38', 22861, NULL, '3578180002.jpg'),
('180-003', '180', 'Kelurahan Tegalsari', '0.53', 22185, NULL, '3578180003.jpg'),
('180-004', '180', 'Kelurahan Wonorejo', '0.68', 26521, NULL, '3578180004.jpg'),
('180-005', '180', 'Kelurahan Kedungdoro', '0.74', 25267, NULL, '3578180005.jpg'),
('190-001', '190', 'Kelurahan Embong Kaliasin', '1.10', 13191, NULL, '3578190001.jpg'),
('190-002', '190', 'Kelurahan Ketabang', '0.98', 7546, NULL, '3578190002.jpg'),
('190-003', '190', 'Kelurahan Genteng', '0.53', 8562, NULL, '3578190003.jpg'),
('190-004', '190', 'Kelurahan Peneleh', '0.45', 15101, NULL, '3578190004.jpg'),
('190-005', '190', 'Kelurahan Kapasari', '0.35', 17534, NULL, '3578190005.jpg'),
('200-001', '200', 'Kelurahan Pacar Keling', '0.70', 23763, NULL, '3578200001.jpg'),
('200-002', '200', 'Kelurahan Pasar Kembang\r\n', '2.09', 41248, NULL, '3578200002.jpg'),
('200-003', '200', 'Kelurahan Ploso', '1.49', 35918, NULL, '3578200003.jpg'),
('200-004', '200', 'Kelurahan Tambak Sari', '0.63', 20696, NULL, '3578200004.jpg'),
('200-005', '200', 'Kelurahan Rangkah', '0.70', 18408, NULL, '3578200005.jpg'),
('200-006', '200', 'Kelurahan Gading', '0.79', 30833, NULL, '3578200006.jpg'),
('200-007', '200', 'Kelurahan Kapas Madya Baru', '1.58', 41596, NULL, '3578200007.jpg'),
('200-008', '200', 'Kelurahan Dukuh Setro', '1.12', 22011, NULL, '3578200008.jpg'),
('210-005', '210', 'Kelurahan Tanah Kalikedinding', '2.41', 58346, NULL, '3578210001.jpg'),
('210-006', '210', 'Kelurahan Sidotopo Wetan', '1.66', 62105, NULL, '3578210002.jpg'),
('210-007', '210', 'Kelurahan Bulak Banteng', '2.67', 34799, NULL, '3578210003.jpg'),
('210-008', '210', 'Kelurahan Tambak Wedi', '0.98', 16924, NULL, '3578210004.jpg'),
('211-001', '211', 'Kelurahan Sukolilo Baru', '2.70', 20097, NULL, '3578211001.jpg'),
('211-002', '211', 'Kelurahan Kenjeran', '0.71', 6076, NULL, '3578211002.jpg'),
('211-003', '211', 'Kelurahan Bulak', '1.32', 6773, NULL, '3578211003.jpg'),
('211-004', '211', 'Kelurahan Kedung Cowek', '0.92', 11222, NULL, '3578211004.jpg'),
('220-001', '220', 'Kelurahan Kapasan', '0.51', 16727, NULL, '3578220001.jpg'),
('220-002', '220', 'Kelurahan Tambak Rejo', '0.61', 20781, NULL, '3578220002.jpg'),
('220-003', '220', 'Kelurahan Simokerto', '0.86', 22952, NULL, '3578220003.jpg'),
('220-004', '220', 'Kelurahan Sidodadi', '0.28', 18247, NULL, '3578220004.jpg'),
('220-005', '220', 'Kelurahan Simolawang', '0.41', 24057, NULL, '3578220005.jpg'),
('230-001', '230', 'Kelurahan Ampel', '0.38', 21124, NULL, '3578230001.jpg'),
('230-002', '230', 'Kelurahan Sidotopo', '2.98', 35372, NULL, '3578230002.jpg'),
('230-003', '230', 'Kelurahan Pegirian', '0.40', 34425, NULL, '3578230003.jpg'),
('230-004', '230', 'Kelurahan Wonokusumo', '0.76', 74665, NULL, '3578230004.jpg'),
('230-005', '230', 'Kelurahan Ujung', '1.62', 36443, NULL, '3578230005.jpg'),
('240-001', '240', 'Kelurahan Bongkaran', '0.90', 12908, NULL, '3578240001.jpg'),
('240-002', '240', 'Kelurahan Nyamplungan', '0.55', 11758, NULL, '3578240002.jpg'),
('240-003', '240', 'Kelurahan Krembangan Utara', '0.68', 18699, NULL, '3578240003.jpg'),
('240-004', '240', 'Kelurahan Perak Timur', '0.40', 16141, NULL, '3578240004.jpg'),
('240-005', '240', 'Kelurahan Perak Utara', '1.79', 30375, NULL, '3578240005.jpg'),
('250-001', '250', 'Kelurahan Tembok Dukuh', '0.83', 27373, NULL, '3578250001.jpg'),
('250-002', '250', 'Kelurahan Bubutan', '0.60', 14724, NULL, '3578250002.jpg'),
('250-003', '250', 'Keluran Alon-alon Contong', '0.65', 7077, NULL, '3578250003.jpg'),
('250-004', '250', 'Kelurahan Gundih', '0.85', 29823, NULL, '3578250004.jpg'),
('250-005', '250', 'Kelurahan Jepara', '0.83', 27402, NULL, '3578250005.jpg'),
('260-001', '260', 'Kelurahan Dupak', '0.48', 25071, NULL, '3578260001.jpg'),
('260-002', '260', 'Kelurahan Morokrembangan', '3.17', 47940, NULL, '3578260002.jpg'),
('260-003', '260', 'Kelurahan Perak Barat', '1.61', 16655, NULL, '3578260003.jpg'),
('260-004', '260', 'Kelurahan Kemayoran', '0.51', 19383, NULL, '3578260004.jpg'),
('260-005', '260', 'Kelurahan Krembangan Selatan', '0.84', 15370, NULL, '3578260005.jpg'),
('270-001', '270', 'Kelurahan Tambak Sarioso', '6.47', 7599, NULL, '3578270001.jpg'),
('270-002', '270', 'Kelurahan Asemrowo', '3.39', 33642, NULL, '3578270002.jpg'),
('270-003', '270', 'Kelurahan Genting Kalianak', '4.07', 8365, NULL, '3578270003.jpg'),
('280-001', '280', 'Kelurahan Sememi', '4.11', 37036, NULL, '3578280001.jpg'),
('280-002', '280', 'Kelurahan Kandangan', '3.61', 22530, NULL, '3578280002.jpg'),
('280-003', '280', 'Kelurahan Tambak Oso Wilangun', '8.46', 3852, NULL, '3578280003.jpg'),
('280-004', '280', 'Kelurahan Romo Kalisari', '7.58', 2644, NULL, '3578280004.jpg'),
('281-001', '281', 'Kelurahan Babat Jerawat', '4.40', 24159, NULL, '3578281001.jpg'),
('281-002', '281', 'Kelurahan Pakal', '3.71', 9622, NULL, '3578281002.jpg'),
('281-003', '281', 'Kelurahan Benowo', '1.98', 11123, NULL, '3578281003.jpg'),
('281-004', '281', 'Kelurahan Sumberejo', '7.50', 11549, NULL, '3578281004.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `rt`
--

CREATE TABLE `rt` (
  `id_rt` varchar(255) NOT NULL,
  `id_kec` varchar(255) DEFAULT NULL,
  `id_kel` varchar(255) DEFAULT NULL,
  `nama_rt` varchar(255) NOT NULL,
  `luas_rt` decimal(6,2) DEFAULT NULL,
  `penduduk_rt` int(7) DEFAULT NULL,
  `image_rt` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rt`
--

INSERT INTO `rt` (`id_rt`, `id_kec`, `id_kel`, `nama_rt`, `luas_rt`, `penduduk_rt`, `image_rt`) VALUES
('010-001-0001', '010', '010-001', 'RT 001/RW 001', NULL, NULL, '35780100010001.jpg'),
('010-001-0002', '010', '010-001', 'RT 002/RW 001', NULL, NULL, '35780100010002.jpg'),
('010-001-0003', '010', '010-001', 'RT 003/RW 001', NULL, NULL, '35780100010003.jpg'),
('010-001-0004', '010', '010-001', 'RT 004/RW 001', NULL, NULL, '35780100010004.jpg'),
('010-001-0005', '010', '010-001', 'RT 005/RW 001', NULL, NULL, '35780100010005.jpg'),
('010-001-0006', '010', '010-001', 'RT 006/RW 001', NULL, NULL, '35780100010006.jpg'),
('010-001-0007', '010', '010-001', 'RT 007/RW 001', NULL, NULL, '35780100010007.jpg'),
('010-001-0008', '010', '010-001', 'RT 001/RW 002', NULL, NULL, '35780100010008.jpg'),
('010-001-0009', '010', '010-001', 'RT 002/RW 002', NULL, NULL, '35780100010009.jpg'),
('010-001-0010', '010', '010-001', 'RT 003/RW 002', NULL, NULL, '35780100010010.jpg'),
('010-001-0011', '010', '010-001', 'RT 004/RW 002', NULL, NULL, '35780100010011.jpg'),
('020-001-0001', '020', '020-001', 'RT 001/RW 001', NULL, NULL, '35780200010001.jpg'),
('020-001-0002', '020', '020-001', 'RT 002/RW 001', NULL, NULL, '35780200010002.jpg'),
('020-001-0003', '020', '020-001', 'RT 003/RW 001', NULL, NULL, '35780200010003.jpg'),
('020-001-0004', '020', '020-001', 'RT 004/RW 001', NULL, NULL, '35780200010004.jpg'),
('020-001-0005', '020', '020-001', 'RT 005/RW 001', NULL, NULL, '35780200010005.jpg'),
('020-001-0006', '020', '020-001', 'RT 006/RW 001', NULL, NULL, '35780200010006.jpg'),
('020-001-0007', '020', '020-001', 'RT 007/RW 001', NULL, NULL, '35780200010007.jpg'),
('020-001-0008', '020', '020-001', 'RT 008/RW 001', NULL, NULL, '35780200010008.jpg'),
('020-001-0009', '020', '020-001', 'RT 009/RW 001', NULL, NULL, '35780200010009.jpg'),
('020-001-0010', '020', '020-001', 'RT 001/RW 002', NULL, NULL, '35780200010010.jpg'),
('020-001-0011', '020', '020-001', 'RT 002/RW 002', NULL, NULL, '35780200010011.jpg'),
('020-001-0012', '020', '020-001', 'RT 003/RW 002', NULL, NULL, '35780200010012.jpg'),
('020-001-0013', '020', '020-001', 'RT 004/RW 002', NULL, NULL, '35780200010013.jpg'),
('030-001-0001', '030', '030-001', 'RT 001/RW 001', NULL, NULL, '35780300010001.jpg'),
('030-001-0002', '030', '030-001', 'RT 002/RW 001', NULL, NULL, '35780300010002.jpg'),
('030-001-0003', '030', '030-001', 'RT 003/RW 001', NULL, NULL, '35780300010003.jpg'),
('030-001-0004', '030', '030-001', 'RT 001/RW 002', NULL, NULL, '35780300010004.jpg'),
('030-001-0005', '030', '030-001', 'RT 002/RW 002', NULL, NULL, '35780300010005.jpg'),
('030-001-0006', '030', '030-001', 'RT 003/RW 002', NULL, NULL, '35780300010006.jpg'),
('030-001-0007', '030', '030-001', 'RT 001/RW 003', NULL, NULL, '35780300010007.jpg'),
('030-001-0008', '030', '030-001', 'RT 002/RW 003', NULL, NULL, '35780300010008.jpg'),
('030-001-0009', '030', '030-001', 'RT 003/RW 003', NULL, NULL, '35780300010009.jpg'),
('030-001-0010', '030', '030-001', 'RT 004/RW 003', NULL, NULL, '35780300010010.jpg'),
('030-001-0011', '030', '030-001', 'RT 005/RW 003', NULL, NULL, '35780300010011.jpg'),
('030-001-0012', '030', '030-001', 'RT 001/RW 004', NULL, NULL, '35780300010012.jpg'),
('030-001-0013', '030', '030-001', 'RT 002/RW 004', NULL, NULL, '35780300010013.jpg'),
('030-001-0014', '030', '030-001', 'RT 003/RW 004', NULL, NULL, '35780300010014.jpg'),
('030-001-0015', '030', '030-001', 'RT 001/RW 005', NULL, NULL, '35780300010015.jpg'),
('030-001-0016', '030', '030-001', 'RT 002/RW 005', NULL, NULL, '35780300010016.jpg'),
('040-001-0001', '040', '040-001', 'RT 001/RW 001', NULL, NULL, '35780400010001.jpg'),
('040-001-0002', '040', '040-001', 'RT 002/RW 001', NULL, NULL, '35780400010002.jpg'),
('040-001-0003', '040', '040-001', 'RT 003/RW 001', NULL, NULL, '35780400010003.jpg'),
('040-001-0004', '040', '040-001', 'RT 004/RW 001', NULL, NULL, '35780400010004.jpg'),
('040-001-0005', '040', '040-001', 'RT 005/RW 001', NULL, NULL, '35780400010005.jpg'),
('040-001-0006', '040', '040-001', 'RT 006/RW 001', NULL, NULL, '35780400010006.jpg'),
('040-001-0007', '040', '040-001', 'RT 007/RW 001', NULL, NULL, '35780400010007.jpg'),
('040-001-0008', '040', '040-001', 'RT 008/RW 001', NULL, NULL, '35780400010008.jpg'),
('040-001-0009', '040', '040-001', 'RT 001/RW 002', NULL, NULL, '35780400010009.jpg'),
('040-001-0010', '040', '040-001', 'RT 002/RW 002', NULL, NULL, '35780400010010.jpg'),
('040-001-0011', '040', '040-001', 'RT 003/RW 002', NULL, NULL, '35780400010011.jpg'),
('050-001-0001', '050', '050-001', 'RT 001/RW 001', NULL, NULL, '35780500010001.jpg'),
('050-001-0002', '050', '050-001', 'RT 002/RW 001', NULL, NULL, '35780500010002.jpg'),
('050-001-0003', '050', '050-001', 'RT 003/RW 001', NULL, NULL, '35780500010003.jpg'),
('050-001-0004', '050', '050-001', 'RT 004/RW 001', NULL, NULL, '35780500010004.jpg'),
('050-001-0005', '050', '050-001', 'RT 005/RW 001', NULL, NULL, '35780500010005.jpg'),
('050-001-0006', '050', '050-001', 'RT 001/RW 002', NULL, NULL, '35780500010006.jpg'),
('050-001-0007', '050', '050-001', 'RT 002/RW 002', NULL, NULL, '35780500010007.jpg'),
('050-001-0008', '050', '050-001', 'RT 003/RW 002', NULL, NULL, '35780500010008.jpg'),
('050-001-0009', '050', '050-001', 'RT 004/RW 002', NULL, NULL, '35780500010009.jpg'),
('050-001-0010', '050', '050-001', 'RT 005/RW 002', NULL, NULL, '35780500010010.jpg'),
('050-001-0011', '050', '050-001', 'RT 006/RW 002', NULL, NULL, '35780500010011.jpg'),
('050-001-0012', '050', '050-001', 'RT 007/RW 002', NULL, NULL, '35780500010012.jpg'),
('050-001-0013', '050', '050-001', 'RT 008/RW 002', NULL, NULL, '35780500010013.jpg'),
('050-001-0014', '050', '050-001', 'RT 009/RW 002', NULL, NULL, '35780500010014.jpg'),
('060-001-0001', '060', '060-001', 'RT 001/RW 001', NULL, NULL, '35780600010001.jpg'),
('060-001-0002', '060', '060-001', 'RT 002/RW 001', NULL, NULL, '35780600010002.jpg'),
('060-001-0003', '060', '060-001', 'RT 003/RW 001', NULL, NULL, '35780600010003.jpg'),
('060-001-0004', '060', '060-001', 'RT 004/RW 001', NULL, NULL, '35780600010004.jpg'),
('060-001-0005', '060', '060-001', 'RT 005/RW 001', NULL, NULL, '35780600010005.jpg'),
('060-001-0006', '060', '060-001', 'RT 006/RW 001', NULL, NULL, '35780600010006.jpg'),
('060-001-0007', '060', '060-001', 'RT 006/RW 001', NULL, NULL, '35780600010007.jpg'),
('060-001-0008', '060', '060-001', 'RT 001/RW 002', NULL, NULL, '35780600010008.jpg'),
('060-001-0009', '060', '060-001', 'RT 002/RW 002', NULL, NULL, '35780600010009.jpg'),
('060-001-0010', '060', '060-001', 'RT 003/RW 002', NULL, NULL, '35780600010010.jpg'),
('060-001-0011', '060', '060-001', 'RT 004/RW 002', NULL, NULL, '35780600010011.jpg'),
('060-001-0012', '060', '060-001', 'RT 005/RW 002', NULL, NULL, '35780600010012.jpg'),
('060-001-0013', '060', '060-001', 'RT 001/RW 003', NULL, NULL, '35780600010013.jpg'),
('060-001-0014', '060', '060-001', 'RT 002/RW 003', NULL, NULL, '35780600010014.jpg'),
('060-001-0015', '060', '060-001', 'RT 003/RW 003', NULL, NULL, '35780600010015.jpg'),
('060-001-0016', '060', '060-001', 'RT 004/RW 003', NULL, NULL, '35780600010016.jpg'),
('060-001-0017', '060', '060-001', 'RT 005/RW 003', NULL, NULL, '35780600010017.jpg'),
('060-001-0018', '060', '060-001', 'RT 006/RW 003', NULL, NULL, '35780600010018.jpg'),
('070-001-0001', '070', '070-001', 'RT 001/RW 001', NULL, NULL, '35780700010001.jpg'),
('070-001-0002', '070', '070-001', 'RT 002/RW 001', NULL, NULL, '35780700010002.jpg'),
('070-001-0003', '070', '070-001', 'RT 003/RW 001', NULL, NULL, '35780700010003.jpg'),
('070-001-0004', '070', '070-001', 'RT 004/RW 001', NULL, NULL, '35780700010004.jpg'),
('070-001-0005', '070', '070-001', 'RT 001/RW 002', NULL, NULL, '35780700010005.jpg'),
('070-001-0006', '070', '070-001', 'RT 002/RW 002', NULL, NULL, '35780700010006.jpg'),
('070-001-0007', '070', '070-001', 'RT 003/RW 002', NULL, NULL, '35780700010007.jpg'),
('070-001-0008', '070', '070-001', 'RT 004/RW 002', NULL, NULL, '35780700010008.jpg'),
('070-001-0009', '070', '070-001', 'RT 005/RW 002', NULL, NULL, '35780700010009.jpg'),
('070-001-0010', '070', '070-001', 'RT 001/RW 003', NULL, NULL, '35780700010010.jpg'),
('070-001-0011', '070', '070-001', 'RT 002/RW 003', NULL, NULL, '35780700010011.jpg'),
('070-001-0012', '070', '070-001', 'RT 003/RW 003', NULL, NULL, '35780700010012.jpg'),
('070-001-0013', '070', '070-001', 'RT 004/RW 003', NULL, NULL, '35780700010013.jpg'),
('070-001-0014', '070', '070-001', 'RT 001/RW 004', NULL, NULL, '35780700010014.jpg'),
('070-001-0015', '070', '070-001', 'RT 002/RW 004', NULL, NULL, '35780700010015.jpg'),
('080-001-0001', '080', '080-001', 'RT 001/RW 001', NULL, NULL, '35780800010001.jpg'),
('080-001-0002', '080', '080-001', 'RT 002/RW 001', NULL, NULL, '35780800010002.jpg'),
('080-001-0003', '080', '080-001', 'RT 003/RW 001', NULL, NULL, '35780800010003.jpg'),
('080-001-0004', '080', '080-001', 'RT 004/RW 001', NULL, NULL, '35780800010004.jpg'),
('080-001-0005', '080', '080-001', 'RT 001/RW 002', NULL, NULL, '35780800010005.jpg'),
('080-001-0006', '080', '080-001', 'RT 002/RW 002', NULL, NULL, '35780800010006.jpg'),
('080-001-0007', '080', '080-001', 'RT 003/RW 002', NULL, NULL, '35780800010007.jpg'),
('080-001-0008', '080', '080-001', 'RT 004/RW 002', NULL, NULL, '35780800010008.jpg'),
('080-001-0009', '080', '080-001', 'RT 001/RW 003', NULL, NULL, '35780800010009.jpg'),
('080-001-0010', '080', '080-001', 'RT 002/RW 003', NULL, NULL, '35780800010010.jpg'),
('080-001-0011', '080', '080-001', 'RT 003/RW 003', NULL, NULL, '35780800010011.jpg'),
('080-001-0012', '080', '080-001', 'RT 004/RW 003', NULL, NULL, '35780800010012.jpg'),
('080-001-0013', '080', '080-001', 'RT 005/RW 003', NULL, NULL, '35780800010013.jpg'),
('080-001-0014', '080', '080-001', 'RT 006/RW 003', NULL, NULL, '35780800010014.jpg'),
('080-001-0015', '080', '080-001', 'RT 001/RW 004', NULL, NULL, '35780800010015.jpg'),
('080-001-0016', '080', '080-001', 'RT 002/RW 004', NULL, NULL, '35780800010016.jpg'),
('080-001-0017', '080', '080-001', 'RT 003/RW 004', NULL, NULL, '35780800010017.jpg'),
('080-001-0018', '080', '080-001', 'RT 004/RW 004', NULL, NULL, '35780800010018.jpg'),
('080-001-0019', '080', '080-001', 'RT 005/RW 004', NULL, NULL, '35780800010019.jpg'),
('090-001-0001', '090', '090-001', 'RT 001/RW 001', NULL, NULL, '35780900010001.jpg'),
('090-001-0002', '090', '090-001', 'RT 002/RW 001', NULL, NULL, '35780900010002.jpg'),
('090-001-0003', '090', '090-001', 'RT 003/RW 001', NULL, NULL, '35780900010003.jpg'),
('090-001-0004', '090', '090-001', 'RT 004/RW 001', NULL, NULL, '35780900010004.jpg'),
('090-001-0005', '090', '090-001', 'RT 005/RW 001', NULL, NULL, '35780900010005.jpg'),
('090-001-0006', '090', '090-001', 'RT 006/RW 001', NULL, NULL, '35780900010006.jpg'),
('090-001-0007', '090', '090-001', 'RT 007/RW 001', NULL, NULL, '35780900010007.jpg'),
('090-001-0008', '090', '090-001', 'RT 001/RW 002', NULL, NULL, '35780900010008.jpg'),
('090-001-0009', '090', '090-001', 'RT 002/RW 002', NULL, NULL, '35780900010009.jpg'),
('090-001-0010', '100', '090-001', 'RT 003/RW 002', NULL, NULL, '35780900010010.jpg'),
('090-001-0011', '090', '090-001', 'RT 004/RW 002', NULL, NULL, '35780900010011.jpg'),
('090-001-0012', '090', '090-001', 'RT 005/RW 002', NULL, NULL, '35780900010012.jpg'),
('090-001-0013', '090', '090-001', 'RT 001/RW 003', NULL, NULL, '35780900010013.jpg'),
('090-001-0014', '090', '090-001', 'RT 002/RW 003', NULL, NULL, '35780900010014.jpg'),
('090-001-0015', '090', '090-001', 'RT 003/RW 003', NULL, NULL, '35780900010015.jpg'),
('090-001-0016', '090', '090-001', 'RT 004/RW 003', NULL, NULL, '35780900010016.jpg'),
('090-001-0017', '090', '090-001', 'RT 005/RW 003', NULL, NULL, '35780900010017.jpg'),
('100-001-0001', '100', '100-001', 'RT 001/RW 001', NULL, NULL, '35781000010001.jpg'),
('100-001-0002', '100', '100-001', 'RT 002/RW 001', NULL, NULL, '35781000010002.jpg'),
('100-001-0003', '100', '100-001', 'RT 003/RW 001', NULL, NULL, '35781000010003.jpg'),
('100-001-0004', '100', '100-001', 'RT 004/RW 001', NULL, NULL, '35781000010004.jpg'),
('100-001-0005', '100', '100-001', 'RT 005/RW 001', NULL, NULL, '35781000010005.jpg'),
('100-001-0006', '100', '100-001', 'RT 006/RW 001', NULL, NULL, '35781000010006.jpg'),
('100-001-0007', '100', '100-001', 'RT 007/RW 001', NULL, NULL, '35781000010007.jpg'),
('100-001-0008', '100', '100-001', 'RT 008/RW 001', NULL, NULL, '35781000010008.jpg'),
('100-001-0009', '100', '100-001', 'RT 001/RW 002', NULL, NULL, '35781000010009.jpg'),
('100-001-0010', '100', '100-001', 'RT 002/RW 002', NULL, NULL, '35781000010010.jpg'),
('100-001-0011', '100', '100-001', 'RT 003/RW 002', NULL, NULL, '35781000010011.jpg'),
('100-001-0012', '100', '100-001', 'RT 004/RW 002', NULL, NULL, '35781000010012.jpg'),
('100-001-0013', '100', '100-001', 'RT 005/RW 002', NULL, NULL, '35781000010013.jpg'),
('100-001-0014', '100', '100-001', 'RT 006/RW 002', NULL, NULL, '35781000010014.jpg'),
('100-001-0015', '100', '100-001', 'RT 007/RW 002', NULL, NULL, '35781000010015.jpg'),
('100-001-0016', '100', '100-001', 'RT 008/RW 002', NULL, NULL, '35781000010016.jpg'),
('100-001-0017', '100', '100-001', 'RT 001/RW 003', NULL, NULL, '35781000010017.jpg'),
('100-001-0018', '100', '100-001', 'RT 002/RW 003', NULL, NULL, '35781000010018.jpg'),
('100-001-0019', '100', '100-001', 'RT 003/RW 003', NULL, NULL, '35781000010019.jpg'),
('100-001-0020', '100', '100-001', 'RT 004/RW 003', NULL, NULL, '35781000010020.jpg'),
('100-001-0022', '100', '100-001', 'RT 006/RW 003', NULL, NULL, '35781000010022.jpg'),
('100-010-0021', '100', '100-001', 'RT 005/RW 003', NULL, NULL, '35781000010021.jpg'),
('110-001-0001', '110', '110-001', 'RT 001/RW 001', NULL, NULL, '35781100010001.jpg'),
('110-001-0002', '110', '110-001', 'RT 002/RW 001', NULL, NULL, '35781100010002.jpg'),
('110-001-0003', '110', '110-001', 'RT 003/RW 001', NULL, NULL, '35781100010003.jpg'),
('110-001-0004', '110', '110-001', 'RT 004/RW 001', NULL, NULL, '35781100010004.jpg'),
('110-001-0005', '110', '110-001', 'RT 005/RW 001', NULL, NULL, '35781100010005.jpg'),
('110-001-0006', '110', '110-001', 'RT 006/RW 001', NULL, NULL, '35781100010006.jpg'),
('110-001-0007', '110', '110-001', 'RT 007/RW 001', NULL, NULL, '35781100010007.jpg'),
('110-001-0008', '110', '110-001', 'RT 008/RW 001', NULL, NULL, '35781100010008.jpg'),
('110-001-0009', '110', '110-001', 'RT 001/RW 002', NULL, NULL, '35781100010009.jpg'),
('110-001-0010', '110', '110-001', 'RT 002/RW 002', NULL, NULL, '35781100010010.jpg'),
('110-001-0011', '110', '110-001', 'RT 003/RW 002', NULL, NULL, '35781100010011.jpg'),
('110-001-0012', '110', '110-001', 'RT 004/RW 002', NULL, NULL, '35781100010012.jpg'),
('110-001-0013', '110', '110-001', 'RT 001/RW 003', NULL, NULL, '35781100010013.jpg'),
('110-001-0014', '110', '110-001', 'RT 002/RW 003', NULL, NULL, '35781100010014.jpg'),
('110-001-0015', '110', '110-001', 'RT 003/RW 003', NULL, NULL, '35781100010015.jpg'),
('110-001-0016', '110', '110-001', 'RT 004/RW 003', NULL, NULL, '35781100010016.jpg'),
('110-001-0017', '110', '110-001', 'RT 005/RW 003', NULL, NULL, '35781100010017.jpg'),
('110-001-0018', '110', '110-001', 'RT 006/RW 003', NULL, NULL, '35781100010018.jpg'),
('110-001-0019', '110', '110-001', 'RT 001/RW 004', NULL, NULL, '35781100010019.jpg'),
('120-001-0001', '120', '120-001', 'RT 001/RW 001', NULL, NULL, '35781200010001.jpg'),
('120-001-0002', '120', '120-001', 'RT 002/RW 001', NULL, NULL, '35781200010002.jpg'),
('120-001-0003', '120', '120-001', 'RT 003/RW 001', NULL, NULL, '35781200010003.jpg'),
('120-001-0004', '120', '120-001', 'RT 001/RW 002', NULL, NULL, '35781200010004.jpg'),
('120-001-0005', '120', '120-001', 'RT 002/RW 002', NULL, NULL, '35781200010005.jpg'),
('120-001-0006', '120', '120-001', 'RT 003/RW 002', NULL, NULL, '35781200010006.jpg'),
('120-001-0007', '120', '120-001', 'RT 004/RW 002', NULL, NULL, '35781200010007.jpg'),
('120-001-0008', '120', '120-001', 'RT 001/RW 003', NULL, NULL, '35781200010008.jpg'),
('120-001-0009', '120', '120-001', 'RT 002/RW 003', NULL, NULL, '35781200010009.jpg'),
('120-001-0010', '120', '120-001', 'RT 003/RW 003', NULL, NULL, '35781200010010.jpg'),
('120-001-0011', '120', '120-001', 'RT 004/RW 003', NULL, NULL, '35781200010011.jpg'),
('120-001-0012', '120', '120-001', 'RT 005/RW 003', NULL, NULL, '35781200010012.jpg'),
('120-001-0013', '120', '120-001', 'RT 006/RW 003', NULL, NULL, '35781200010013.jpg'),
('120-001-0014', '120', '120-001', 'RT 007/RW 003', NULL, NULL, '35781200010014.jpg'),
('120-001-0015', '120', '120-001', 'RT 001/RW 004', NULL, NULL, '35781200010015.jpg'),
('120-001-0016', '120', '120-001', 'RT 002/RW 004', NULL, NULL, '35781200010016.jpg'),
('120-001-0017', '120', '120-001', 'RT 003/RW 004', NULL, NULL, '35781200010017.jpg'),
('120-001-0018', '120', '120-001', 'RT 004/RW 004', NULL, NULL, '35781200010018.jpg'),
('120-001-0019', '120', '120-001', 'RT 005/RW 004', NULL, NULL, '35781200010019.jpg'),
('120-001-0020', '120', '120-001', 'RT 006/RW 004', NULL, NULL, '35781200010020.jpg'),
('120-001-0021', '120', '120-001', 'RT 001/RW 005', NULL, NULL, '35781200010021.jpg'),
('130-001-0001', '130', '130-001', 'RT 001/RW 001', NULL, NULL, '35781300010001.jpg'),
('130-001-0002', '130', '130-001', 'RT 002/RW 001', NULL, NULL, '35781300010002.jpg'),
('130-001-0003', '130', '130-001', 'RT 003/RW 001', NULL, NULL, '35781300010003.jpg'),
('130-001-0004', '130', '130-001', 'RT 001/RW 002', NULL, NULL, '35781300010004.jpg'),
('130-001-0005', '130', '130-001', 'RT 002/RW 002', NULL, NULL, '35781300010005.jpg'),
('130-001-0006', '130', '130-001', 'RT 003/RW 002', NULL, NULL, '35781300010006.jpg'),
('130-001-0007', '130', '130-001', 'RT 004/RW 002', NULL, NULL, '35781300010007.jpg'),
('130-001-0008', '130', '130-001', 'RT 005/RW 002', NULL, NULL, '35781300010008.jpg'),
('130-001-0009', '130', '130-001', 'RT 006/RW 002', NULL, NULL, '35781300010009.jpg'),
('130-001-0010', '130', '130-001', 'RT 001/RW 003', NULL, NULL, '35781300010010.jpg'),
('130-001-0011', '130', '130-001', 'RT 002/RW 003', NULL, NULL, '35781300010011.jpg'),
('130-001-0012', '130', '130-001', 'RT 003/RW 003', NULL, NULL, '35781300010012.jpg'),
('130-001-0013', '130', '130-001', 'RT 004/RW 003', NULL, NULL, '35781300010013.jpg'),
('130-001-0014', '130', '130-001', 'RT 001/RW 004', NULL, NULL, '35781300010014.jpg'),
('130-001-0015', '130', '130-001', 'RT 002/RW 004', NULL, NULL, '35781300010015.jpg'),
('140-001-0001', '140', '140-001', 'RT 001/RW 001', NULL, NULL, '35781400010001.jpg'),
('140-001-0002', '140', '140-001', 'RT 002/RW 001', NULL, NULL, '35781400010002.jpg'),
('140-001-0003', '140', '140-001', 'RT 003/RW 001', NULL, NULL, '35781400010003.jpg'),
('140-001-0004', '140', '140-001', 'RT 004/RW 001', NULL, NULL, '35781400010004.jpg'),
('140-001-0005', '140', '140-001', 'RT 005/RW 001', NULL, NULL, '35781400010005.jpg'),
('140-001-0006', '140', '140-001', 'RT 006/RW 001', NULL, NULL, '35781400010006.jpg'),
('140-001-0007', '140', '140-001', 'RT 001/RW 002', NULL, NULL, '35781400010007.jpg'),
('140-001-0008', '140', '140-001', 'RT 002/RW 002', NULL, NULL, '35781400010008.jpg'),
('140-001-0009', '140', '140-001', 'RT 003/RW 002', NULL, NULL, '35781400010009.jpg'),
('140-001-0010', '140', '140-001', 'RT 004/RW 002', NULL, NULL, '35781400010010.jpg'),
('140-001-0011', '140', '140-001', 'RT 001/RW 003', NULL, NULL, '35781400010011.jpg'),
('140-001-0012', '140', '140-001', 'RT 002/RW 003', NULL, NULL, '35781400010012.jpg'),
('140-001-0013', '140', '140-001', 'RT 003/RW 003', NULL, NULL, '35781400010013.jpg'),
('140-001-0014', '140', '140-001', 'RT 004/RW 003', NULL, NULL, '35781400010014.jpg'),
('140-001-0015', '140', '140-001', 'RT 005/RW 003', NULL, NULL, '35781400010015.jpg'),
('140-001-0016', '140', '140-001', 'RT 001/RW 004', NULL, NULL, '35781400010016.jpg'),
('140-001-0017', '140', '140-001', 'RT 002/RW 004', NULL, NULL, '35781400010017.jpg'),
('140-001-0018', '140', '140-001', 'RT 003/RW 004', NULL, NULL, '35781400010018.jpg'),
('140-001-0019', '140', '140-001', 'RT 004/RW 004', NULL, NULL, '35781400010019.jpg'),
('140-001-0020', '140', '140-001', 'RT 005/RW 004', NULL, NULL, '35781400010020.jpg'),
('140-001-0021', '140', '140-001', 'RT 006/RW 004', NULL, NULL, '35781400010021.jpg'),
('140-001-0022', '140', '140-001', 'RT 007/RW 004', NULL, NULL, '35781400010022.jpg'),
('140-001-0023', '140', '140-001', 'RT 008/RW 004', NULL, NULL, '35781400010023.jpg'),
('141-001-0001', '141', '141-001', 'RT 001/RW 001', NULL, NULL, '35781410010001.jpg'),
('141-001-0002', '141', '141-001', 'RT 002/RW 001', NULL, NULL, '35781410010002.jpg'),
('141-001-0003', '141', '141-001', 'RT 003/RW 001', NULL, NULL, '35781410010003.jpg'),
('141-001-0004', '141', '141-001', 'RT 004/RW 001', NULL, NULL, '35781410010004.jpg'),
('141-001-0005', '141', '141-001', 'RT 001/RW 002', NULL, NULL, '35781410010005.jpg'),
('141-001-0006', '141', '141-001', 'RT 002/RW 002', NULL, NULL, '35781410010006.jpg'),
('141-001-0007', '141', '141-001', 'RT 003/RW 002', NULL, NULL, '35781410010007.jpg'),
('141-001-0008', '141', '141-001', 'RT 001/RW 003', NULL, NULL, '35781410010008.jpg'),
('141-001-0009', '141', '141-001', 'RT 002/RW 003', NULL, NULL, '35781410010009.jpg'),
('141-001-0010', '141', '141-001', 'RT 003/RW 003', NULL, NULL, '35781410010010.jpg'),
('141-001-0011', '141', '141-001', 'RT 004/RW 003', NULL, NULL, '35781410010011.jpg'),
('141-001-0012', '141', '141-001', 'RT 001/RW 004', NULL, NULL, '35781410010012.jpg'),
('141-001-0013', '141', '141-001', 'RT 002/RW 004', NULL, NULL, '35781410010013.jpg'),
('141-001-0014', '141', '141-001', 'RT 003/RW 004', NULL, NULL, '35781410010014.jpg'),
('150-006-0001', '150', '150-006', 'RT 001/RW 001', NULL, NULL, '35781500060001.jpg'),
('150-006-0002', '150', '150-006', 'RT 002/RW 001', NULL, NULL, '35781500060002.jpg'),
('150-006-0003', '150', '150-006', 'RT 003/RW 001', NULL, NULL, '35781500060003.jpg'),
('150-006-0004', '150', '150-001', 'RT 004/RW 001', NULL, NULL, '35781500060004.jpg'),
('150-006-0005', '150', '150-001', 'RT 005/RW 001', NULL, NULL, '35781500060005.jpg'),
('150-006-0006', '150', '150-006', 'RT 006/RW 001', NULL, NULL, '35781500060006.jpg'),
('150-006-0007', '150', '150-006', 'RT 007/RW 001', NULL, NULL, '35781500060007.jpg'),
('150-006-0008', '150', '150-006', 'RT 001/RW 002', NULL, NULL, '35781500060008.jpg'),
('150-006-0009', '150', '150-006', 'RT 002/RW 002', NULL, NULL, '35781500060009.jpg'),
('150-006-0010', '150', '150-001', 'RT 003/RW 002', NULL, NULL, '35781500060010.jpg'),
('150-006-0011', '150', '150-006', 'RT 004/RW 002', NULL, NULL, '35781500060011.jpg'),
('150-006-0012', '150', '150-006', 'RT 001/RW 003', NULL, NULL, '35781500060012.jpg'),
('150-006-0013', '150', '150-006', 'RT 002/RW 003', NULL, NULL, '35781500060013.jpg'),
('150-006-0014', '150', '150-006', 'RT 003/RW 003', NULL, NULL, '35781500060014.jpg'),
('150-006-0015', '150', '150-006', 'RT 004/RW 003', NULL, NULL, '35781500060015.jpg'),
('150-006-0016', '150', '150-006', 'RT 001/RW 004', NULL, NULL, '35781500060016.jpg'),
('150-006-0017', '150', '150-006', 'RT 002/RW 004', NULL, NULL, '35781500060017.jpg'),
('150-006-0018', '150', '150-006', 'RT 003/RW 004', NULL, NULL, '35781500060018.jpg'),
('150-006-0019', '150', '150-006', 'RT 004/RW 004', NULL, NULL, '35781500060019.jpg'),
('150-006-0020', '150', '150-006', 'RT 001/RW 005', NULL, NULL, '35781500060020.jpg'),
('150-006-0021', '150', '150-006', 'RT 002/RW 005', NULL, NULL, '35781500060021.jpg'),
('150-006-0022', '150', '150-006', 'RT 003/RW 005', NULL, NULL, '35781500060022.jpg'),
('150-006-0023', '150', '150-006', 'RT 004/RW 005', NULL, NULL, '35781500060023.jpg'),
('150-006-0024', '150', '150-006', 'RT 005/RW 005', NULL, NULL, '35781500060024.jpg'),
('150-006-0025', '150', '150-006', 'RT 006/RW 005', NULL, NULL, '35781500060025.jpg'),
('150-006-0026', '150', '150-006', 'RT 001/RW 006', NULL, NULL, '35781500060026.jpg'),
('150-006-0027', '150', '150-006', 'RT 002/RW 006', NULL, NULL, '35781500060027.jpg'),
('160-001-0001', '160', '160-001', 'RT 001/RW 001', NULL, NULL, '35781600010001.jpg'),
('160-001-0002', '160', '160-001', 'RT 002/RW 001', NULL, NULL, '35781600010002.jpg'),
('160-001-0003', '160', '160-001', 'RT 003/RW 001', NULL, NULL, '35781600010003.jpg'),
('160-001-0004', '160', '160-001', 'RT 004/RW 001', NULL, NULL, '35781600010004.jpg'),
('160-001-0005', '160', '160-001', 'RT 005/RW 001', NULL, NULL, '35781600010005.jpg'),
('170-001-0001', '170', '170-001', 'RT 001/RW 001', NULL, NULL, '35781700010001.jpg'),
('170-001-0002', '170', '170-001', 'RT 002/RW 001', NULL, NULL, '35781700010002.jpg'),
('170-001-0003', '170', '170-001', 'RT 003/RW 001', NULL, NULL, '35781700010003.jpg'),
('170-001-0004', '170', '170-001', 'RT 004/RW 001', NULL, NULL, '35781700010004.jpg'),
('170-001-0005', '170', '170-001', 'RT 001/RW 002', NULL, NULL, '35781700010005.jpg'),
('170-001-0006', '170', '170-001', 'RT 002/RW 002', NULL, NULL, '35781700010007.jpg'),
('170-001-0007', '170', '170-001', 'RT 003/RW 002', NULL, NULL, '35781700010007.jpg'),
('170-001-0008', '170', '170-001', 'RT 004/RW 002', NULL, NULL, '35781700010008.jpg'),
('170-001-0009', '170', '170-001', 'RT 005/RW 002', NULL, NULL, '35781700010009.jpg'),
('170-001-0010', '170', '170-001', 'RT 006/RW 002', NULL, NULL, '35781700010010.jpg'),
('170-001-0011', '170', '170-001', 'RT 001/RW 003', NULL, NULL, '35781700010011.jpg'),
('170-001-0012', '170', '170-001', 'RT 002/RW 003', NULL, NULL, '35781700010012.jpg'),
('170-001-0013', '170', '170-001', 'RT 003/RW 003', NULL, NULL, '35781700010013.jpg'),
('170-001-0014', '170', '170-001', 'RT 004/RW 003', NULL, NULL, '35781700010014.jpg'),
('170-001-0015', '170', '170-001', 'RT 005/RW 003', NULL, NULL, '35781700010015.jpg'),
('170-001-0016', '170', '170-001', 'RT 006/RW 003', NULL, NULL, '35781700010016.jpg'),
('170-001-0017', '170', '170-001', 'RT 007/RW 003', NULL, NULL, '35781700010017.jpg'),
('170-001-0018', '170', '170-001', 'RT 008/RW 003', NULL, NULL, '35781700010018.jpg'),
('170-001-0019', '170', '170-001', 'RT 009/RW 003', NULL, NULL, '35781700010019.jpg'),
('170-001-0020', '170', '170-001', 'RT 010/RW 003', NULL, NULL, '35781700010020.jpg'),
('170-001-0021', '170', '170-001', 'RT 011/RW 003', NULL, NULL, '35781700010021.jpg'),
('180-001-0001', '180', '180-001', 'RT 001/RW 001', NULL, NULL, '35781800010001.jpg'),
('180-001-0002', '180', '180-001', 'RT 002/RW 001', NULL, NULL, '35781800010002.jpg'),
('180-001-0003', '180', '180-001', 'RT 003/RW 001', NULL, NULL, '35781800010003.jpg'),
('180-001-0004', '180', '180-001', 'RT 004/RW 001', NULL, NULL, '35781800010004.jpg'),
('180-001-0005', '180', '180-001', 'RT 005/RW 001', NULL, NULL, '35781800010005.jpg'),
('180-001-0006', '180', '180-001', 'RT 006/RW 001', NULL, NULL, '35781800010006.jpg'),
('180-001-0007', '180', '180-001', 'RT 007/RW 001', NULL, NULL, '35781800010007.jpg'),
('180-001-0008', '180', '180-001', 'RT 008/RW 001', NULL, NULL, '35781800010008.jpg'),
('180-001-0009', '180', '180-001', 'RT 001/RW 002', NULL, NULL, '35781800010009.jpg'),
('180-001-0010', '180', '180-001', 'RT 002/RW 002', NULL, NULL, '35781800010010.jpg'),
('180-001-0011', '180', '180-001', 'RT 003/RW 002', NULL, NULL, '35781800010011.jpg'),
('180-001-0012', '180', '180-001', 'RT 004/RW 002', NULL, NULL, '35781800010012.jpg'),
('180-001-0013', '180', '180-001', 'RT 005/RW 002', NULL, NULL, '35781800010013.jpg'),
('180-001-0014', '180', '180-001', 'RT 001/RW 003', NULL, NULL, '35781800010014.jpg'),
('180-001-0015', '180', '180-001', 'RT 002/RW 003', NULL, NULL, '35781800010015.jpg'),
('180-001-0016', '180', '180-001', 'RT 003/RW 003', NULL, NULL, '35781800010016.jpg'),
('180-001-0017', '180', '180-001', 'RT 004/RW 003', NULL, NULL, '35781800010017.jpg'),
('190-001-0001', '190', '190-001', 'RT 001/RW 001', NULL, NULL, '35781900010001.jpg'),
('190-001-0002', '190', '190-001', 'RT 002/RW 001', NULL, NULL, '35781900010002.jpg'),
('190-001-0003', '190', '190-001', 'RT 003/RW 001', NULL, NULL, '35781900010003.jpg'),
('190-001-0004', '190', '190-001', 'RT 004/RW 001', NULL, NULL, '35781900010004.jpg'),
('190-001-0005', '190', '190-001', 'RT 002/RW 002', NULL, NULL, '35781900010005.jpg'),
('190-001-0006', '190', '190-001', 'RT 004/RW 002', NULL, NULL, '35781900010006.jpg'),
('190-001-0007', '190', '190-001', 'RT 005/RW 002', NULL, NULL, '35781900010007.jpg'),
('190-001-0008', '190', '190-001', 'RT 001/RW 003', NULL, NULL, '35781900010008.jpg'),
('190-001-0009', '190', '190-001', 'RT 002/RW 003', NULL, NULL, '35781900010009.jpg'),
('190-001-0010', '190', '190-001', 'RT 003/RW 003', NULL, NULL, '35781900010010.jpg'),
('190-001-0011', '190', '190-001', 'RT 001/RW 004', NULL, NULL, '35781900010011.jpg'),
('190-001-0012', '190', '190-001', 'RT 002/RW 004', NULL, NULL, '35781900010012.jpg'),
('190-001-0013', '190', '190-001', 'RT 003/RW 004', NULL, NULL, '35781900010013.jpg'),
('190-001-0014', '190', '190-001', 'RT 001/RW 005', NULL, NULL, '35781900010014.jpg'),
('190-001-0015', '190', '190-001', 'RT 002/RW 005', NULL, NULL, '35781900010015.jpg'),
('190-001-0016', '190', '190-001', 'RT 003/RW 005', NULL, NULL, '35781900010016.jpg'),
('190-001-0017', '190', '190-001', 'RT 001/RW 007', NULL, NULL, '35781900010017.jpg'),
('190-001-0018', '190', '190-001', 'RT 002/RW 007', NULL, NULL, '35781900010018.jpg'),
('190-001-0019', '190', '190-001', 'RT 003/RW 007', NULL, NULL, '35781900010019.jpg'),
('190-001-0020', '190', '190-001', 'RT 001/RW 008', NULL, NULL, '35781900010020.jpg'),
('190-001-0021', '190', '190-001', 'RT 002/RW 008', NULL, NULL, '35781900010021.jpg'),
('200-001-0001', '200', '200-001', 'RT 001/RW 001', NULL, NULL, '35782000010001.jpg'),
('200-001-0002', '200', '200-001', 'RT 002/RW 001', NULL, NULL, '35782000010002.jpg'),
('200-001-0003', '200', '200-001', 'RT 003/RW 001', NULL, NULL, '35782000010003.jpg'),
('200-001-0004', '200', '200-001', 'RT 004/RW 001', NULL, NULL, '35782000010004.jpg'),
('200-001-0005', '200', '200-001', 'RT 005/RW 001', NULL, NULL, '35782000010005.jpg'),
('200-001-0006', '200', '200-001', 'RT 006/RW 001', NULL, NULL, '35782000010006.jpg'),
('200-001-0007', '200', '200-001', 'RT 007/RW 001', NULL, NULL, '35782000010007.jpg'),
('200-001-0008', '200', '200-001', 'RT 008/RW 001', NULL, NULL, '35782000010008.jpg'),
('200-001-0009', '200', '200-001', 'RT 009/RW 001', NULL, NULL, '35782000010009.jpg'),
('200-001-0010', '200', '200-001', 'RT 001/RW 002', NULL, NULL, '35782000010010.jpg'),
('200-001-0011', '200', '200-001', 'RT 002/RW 002', NULL, NULL, '35782000010011.jpg'),
('200-001-0012', '200', '200-001', 'RT 003/RW 002', NULL, NULL, '35782000010012.jpg'),
('200-001-0013', '200', '200-001', 'RT 004/RW 002', NULL, NULL, '35782000010013.jpg'),
('200-001-0014', '200', '200-001', 'RT 005/RW 002', NULL, NULL, '35782000010014.jpg'),
('200-001-0015', '200', '200-001', 'RT 006/RW 002', NULL, NULL, '35782000010015.jpg'),
('200-001-0016', '200', '200-001', 'RT 001/RW 003', NULL, NULL, '35782000010016.jpg'),
('200-001-0017', '200', '200-001', 'RT 002/RW 003', NULL, NULL, '35782000010017.jpg'),
('200-001-0018', '200', '200-001', 'RT 003/RW 003', NULL, NULL, '35782000010018.jpg'),
('200-001-0019', '200', '200-001', 'RT 004/RW 003', NULL, NULL, '35782000010019.jpg'),
('210-005-0001', '210', '210-005', 'RT 001/RW 001', NULL, NULL, '35782100050001.jpg'),
('210-005-0002', '210', '210-005', 'RT 002/RW 001', NULL, NULL, '35782100050002.jpg'),
('210-005-0003', '210', '210-005', 'RT 003/RW 001', NULL, NULL, '35782100050003.jpg'),
('210-005-0004', '210', '210-005', 'RT 004/RW 001', NULL, NULL, '35782100050004.jpg'),
('210-005-0005', '210', '210-005', 'RT 005/RW 001', NULL, NULL, '35782100050005.jpg'),
('210-005-0006', '210', '210-005', 'RT 006/RW 001', NULL, NULL, '35782100050006.jpg'),
('210-005-0007', '210', '210-005', 'RT 007/RW 001', NULL, NULL, '35782100050007.jpg'),
('210-005-0008', '210', '210-005', 'RT 008/RW 001', NULL, NULL, '35782100050008.jpg'),
('210-005-0009', '210', '210-005', 'RT 009/RW 001', NULL, NULL, '35782100050009.jpg'),
('210-005-0010', '210', '210-005', 'RT 010/RW 001', NULL, NULL, '35782100050010.jpg'),
('210-005-0011', '210', '210-005', 'RT 011/RW 001', NULL, NULL, '35782100050011.jpg'),
('210-005-0012', '210', '210-005', 'RT 012/RW 001', NULL, NULL, '35782100050012.jpg'),
('210-005-0013', '210', '210-005', 'RT 014/RW 001', NULL, NULL, '35782100050013.jpg'),
('210-005-0014', '210', '210-005', 'RT 015/RW 001', NULL, NULL, '35782100050014.jpg'),
('210-005-0015', '210', '210-005', 'RT 016/RW 001', NULL, NULL, '35782100050015.jpg'),
('210-005-0016', '210', '210-005', 'RT 017/RW 001', NULL, NULL, '35782100050016.jpg'),
('210-005-0017', '210', '210-005', 'RT 018/RW 001', NULL, NULL, '35782100050017.jpg'),
('210-005-0018', '210', '210-005', 'RT 019/RW 001', NULL, NULL, '35782100050018.jpg'),
('210-005-0019', '210', '210-005', 'RT 020/RW 001', NULL, NULL, '35782100050019.jpg'),
('210-005-0020', '210', '210-005', 'RT 021/RW 001', NULL, NULL, '35782100050020.jpg'),
('211-003-0001', '211', '211-003', 'RT 001/RW 001', NULL, NULL, '35782110030001.jpg'),
('211-003-0002', '211', '211-003', 'RT 002/RW 001', NULL, NULL, '35782110030002.jpg'),
('211-003-0003', '211', '211-003', 'RT 003/RW 001', NULL, NULL, '35782110030003.jpg'),
('211-003-0004', '211', '211-003', 'RT 004/RW 001', NULL, NULL, '35782110030004.jpg'),
('211-003-0005', '211', '211-003', 'RT 005/RW 001', NULL, NULL, '35782110030005.jpg'),
('211-003-0006', '211', '211-003', 'RT 006/RW 001', NULL, NULL, '35782110030006.jpg'),
('211-003-0007', '211', '211-003', 'RT 007/RW 001', NULL, NULL, '35782110030007.jpg'),
('220-001-0001', '220', '220-001', 'RT 001/RW 001', NULL, NULL, '35782200010001.jpg'),
('220-001-0002', '220', '220-001', 'RT 002/RW 001', NULL, NULL, '35782200010002.jpg'),
('220-001-0003', '220', '220-001', 'RT 003/RW 001', NULL, NULL, '35782200010003.jpg'),
('220-001-0004', '220', '220-001', 'RT 004/RW 001', NULL, NULL, '35782200010004.jpg'),
('220-001-0005', '220', '220-001', 'RT 005/RW 001', NULL, NULL, '35782200010005.jpg'),
('220-001-0006', '220', '220-001', 'RT 001/RW 002', NULL, NULL, '35782200010006.jpg'),
('220-001-0007', '220', '220-001', 'RT 002/RW 002', NULL, NULL, '35782200010007.jpg'),
('220-001-0008', '220', '220-001', 'RT 003/RW 002', NULL, NULL, '35782200010008.jpg'),
('220-001-0009', '220', '220-005', 'RT 004/RW 002', NULL, NULL, '35782200010009.jpg'),
('220-001-0010', '220', '220-001', 'RT 005/RW 002', NULL, NULL, '35782200010010.jpg'),
('220-001-0011', '220', '220-001', 'RT 001/RW 003', NULL, NULL, '35782200010011.jpg'),
('220-001-0012', '220', '220-001', 'RT 002/RW 003', NULL, NULL, '35782200010012.jpg'),
('220-001-0013', '220', '220-001', 'RT 003/RW 003', NULL, NULL, '35782200010013.jpg'),
('220-001-0014', '220', '220-001', 'RT 004/RW 003', NULL, NULL, '35782200010014.jpg'),
('220-001-0015', '220', '220-001', 'RT 005/RW 003', NULL, NULL, '35782200010015.jpg'),
('220-001-0016', '220', '220-001', 'RT 006/RW 003', NULL, NULL, '35782200010016.jpg'),
('220-001-0017', '220', '220-001', 'RT 007/RW 003', NULL, NULL, '35782200010017.jpg'),
('220-001-0018', '220', '220-001', 'RT 001/RW 004', NULL, NULL, '35782200010018.jpg'),
('220-001-0019', '220', '220-001', 'RT 002/RW 004', NULL, NULL, '35782200010019.jpg'),
('220-001-0020', '220', '220-001', 'RT 003/RW 004', NULL, NULL, '35782200010020.jpg'),
('220-001-0021', '220', '220-001', 'RT 004/RW 004', NULL, NULL, '35782200010021.jpg'),
('220-001-0022', '220', '220-001', 'RT 005/RW 004', NULL, NULL, '35782200010022.jpg'),
('220-001-0023', '220', '220-001', 'RT 006/RW 004', NULL, NULL, '35782200010023.jpg'),
('230-001-0001', '230', '230-001', 'RT 001/RW 001', NULL, NULL, '35782300010001.jpg'),
('230-001-0002', '230', '230-001', 'RT 002/RW 001', NULL, NULL, '35782300010002.jpg'),
('230-001-0003', '230', '230-001', 'RT 003/RW 001', NULL, NULL, '35782300010003.jpg'),
('240-001-0001', '240', '240-001', 'RT 001/RW 001', NULL, NULL, '35782400010001.jpg'),
('240-001-0002', '240', '240-001', 'RT 002/RW 001', NULL, NULL, '35782400010002.jpg'),
('240-001-0003', '240', '240-001', 'RT 003/RW 001', NULL, NULL, '35782400010003.jpg'),
('240-001-0004', '240', '240-001', 'RT 001/RW 002', NULL, NULL, '35782400010004.jpg'),
('240-001-0005', '240', '240-001', 'RT 002/RW 002', NULL, NULL, '35782400010005.jpg'),
('240-001-0006', '240', '240-001', 'RT 003/RW 002', NULL, NULL, '35782400010006.jpg'),
('240-001-0007', '240', '240-001', 'RT 001/RW 003', NULL, NULL, '35782400010007.jpg'),
('240-001-0008', '240', '240-001', 'RT 002/RW 003', NULL, NULL, '35782400010008.jpg'),
('240-001-0009', '240', '240-001', 'RT 003/RW 003', NULL, NULL, '35782400010009.jpg'),
('240-001-0010', '240', '240-001', 'RT 004/RW 003', NULL, NULL, '35782400010010.jpg'),
('240-001-0011', '240', '240-001', 'RT 005/RW 003', NULL, NULL, '35782400010011.jpg'),
('240-001-0012', '240', '240-001', 'RT 001/RW 004', NULL, NULL, '35782400010012.jpg'),
('240-001-0013', '240', '240-001', 'RT 002/RW 004', NULL, NULL, '35782400010013.jpg'),
('240-001-0014', '240', '240-001', 'RT 003/RW 004', NULL, NULL, '35782400010014.jpg'),
('240-001-0015', '240', '240-001', 'RT 001/RW 005', NULL, NULL, '35782400010015.jpg'),
('240-001-0016', '240', '240-001', 'RT 002/RW 005', NULL, NULL, '35782400010016.jpg'),
('240-001-0017', '240', '240-001', 'RT 003/RW 005', NULL, NULL, '35782400010017.jpg'),
('250-001-0001', '250', '250-001', 'RT 001/RW 001', NULL, NULL, '35782500010001.jpg'),
('250-001-0002', '250', '250-001', 'RT 002/RW 001', NULL, NULL, '35782500010002.jpg'),
('250-001-0003', '250', '250-001', 'RT 003/RW 001', NULL, NULL, '35782500010003.jpg'),
('250-001-0004', '250', '250-001', 'RT 004/RW 001', NULL, NULL, '35782500010004.jpg'),
('250-001-0005', '250', '250-001', 'RT 005/RW 001', NULL, NULL, '35782500010005.jpg'),
('250-001-0006', '250', '250-001', 'RT 006/RW 001', NULL, NULL, '35782500010006.jpg'),
('250-001-0007', '250', '250-001', 'RT 007/RW 001', NULL, NULL, '35782500010007.jpg'),
('250-001-0008', '250', '250-001', 'RT 008/RW 001', NULL, NULL, '35782500010008.jpg'),
('250-001-0009', '250', '250-001', 'RT 009/RW 001', NULL, NULL, '35782500010009.jpg'),
('250-001-0010', '250', '250-001', 'RT 010/RW 001', NULL, NULL, '35782500010010.jpg'),
('250-001-0011', '250', '250-001', 'RT 011/RW 001', NULL, NULL, '35782500010011.jpg'),
('250-001-0012', '250', '250-001', 'RT 012/RW 001', NULL, NULL, '35782500010012.jpg'),
('250-001-0013', '250', '250-001', 'RT 013/RW 001', NULL, NULL, '35782500010013.jpg'),
('250-001-0014', '250', '250-001', 'RT 001/RW 002', NULL, NULL, '35782500010014.jpg'),
('250-001-0015', '250', '250-001', 'RT 002/RW 002', NULL, NULL, '35782500010015.jpg'),
('250-001-0016', '250', '250-001', 'RT 003/RW 002', NULL, NULL, '35782500010016.jpg'),
('250-001-0017', '250', '250-001', 'RT 004/RW 002', NULL, NULL, '35782500010017.jpg'),
('250-001-0018', '250', '250-001', 'RT 005/RW 002', NULL, NULL, '35782500010018.jpg'),
('250-001-0019', '250', '250-001', 'RT 006/RW 002', NULL, NULL, '35782500010019.jpg'),
('260-001-0001', '260', '260-001', 'RT 001/RW 001', NULL, NULL, '35782600010001.jpg'),
('260-001-0002', '260', '260-001', 'RT 002/RW 001', NULL, NULL, '35782600010002.jpg'),
('260-001-0003', '260', '260-001', 'RT 003/RW 001', NULL, NULL, '35782600010003.jpg'),
('260-001-0004', '260', '260-001', 'RT 004/RW 001', NULL, NULL, '35782600010004.jpg'),
('260-001-0005', '260', '260-001', 'RT 005/RW 001', NULL, NULL, '35782600010005.jpg'),
('260-001-0006', '260', '260-001', 'RT 006/RW 001', NULL, NULL, '35782600010006.jpg'),
('260-001-0007', '260', '260-001', 'RT 007/RW 001', NULL, NULL, '35782600010007.jpg'),
('260-001-0008', '260', '260-001', 'RT 008/RW 001', NULL, NULL, '35782600010008.jpg'),
('260-001-0009', '260', '260-001', 'RT 009/RW 001', NULL, NULL, '35782600010009.jpg'),
('260-001-0010', '260', '260-001', 'RT 010/RW 001', NULL, NULL, '35782600010010.jpg'),
('260-001-0011', '260', '260-001', 'RT 001/RW 002', NULL, NULL, '35782600010011.jpg'),
('260-001-0012', '260', '260-001', 'RT 002/RW 002', NULL, NULL, '35782600010012.jpg'),
('260-001-0013', '260', '260-001', 'RT 003/RW 002', NULL, NULL, '35782600010013.jpg'),
('260-001-0014', '260', '260-001', 'RT 004/RW 002', NULL, NULL, '35782600010014.jpg'),
('260-001-0015', '260', '260-001', 'RT 005/RW 002', NULL, NULL, '35782600010015.jpg'),
('260-001-0016', '260', '260-001', 'RT 006/RW 002', NULL, NULL, '35782600010016.jpg'),
('260-001-0017', '260', '260-001', 'RT 007/RW 002', NULL, NULL, '35782600010017.jpg'),
('260-001-0018', '260', '260-001', 'RT 008/RW 002', NULL, NULL, '35782600010018.jpg'),
('260-001-0019', '260', '260-001', 'RT 009/RW 002', NULL, NULL, '35782600010019.jpg'),
('260-001-0020', '260', '260-001', 'RT 011/RW 002', NULL, NULL, '35782600010020.jpg'),
('260-001-0021', '260', '260-001', 'RT 012/RW 002', NULL, NULL, '35782600010021.jpg'),
('260-001-0022', '260', '260-001', 'RT 013/RW 002', NULL, NULL, '35782600010022.jpg'),
('260-001-0023', '260', '260-001', 'RT 014/RW 002', NULL, NULL, '35782600010023.jpg'),
('260-001-0024', '260', '260-001', 'RT 015/RW 002', NULL, NULL, '35782600010024.jpg'),
('260-001-0025', '260', '260-001', 'RT 016/RW 002', NULL, NULL, '35782600010025.jpg'),
('260-001-0026', '260', '260-001', 'RT 017/RW 001', NULL, NULL, '35782600010026.jpg'),
('260-001-0027', '260', '260-001', 'RT 018/RW 002', NULL, NULL, '35782600010027.jpg'),
('270-003-0001', '270', '270-003', 'RT 001/RW 001', NULL, NULL, '35782700030001.jpg'),
('270-003-0002', '270', '270-003', 'RT 002/RW 001', NULL, NULL, '35782700030002.jpg'),
('270-003-0003', '270', '270-003', 'RT 003/RW 001', NULL, NULL, '35782700030003.jpg'),
('270-003-0004', '270', '270-003', 'RT 004/RW 001', NULL, NULL, '35782700030004.jpg'),
('270-003-0005', '270', '270-003', 'RT 005/RW 001', NULL, NULL, '35782700030005.jpg'),
('270-003-0006', '270', '270-003', 'RT 006/RW 001', NULL, NULL, '35782700030006.jpg'),
('270-003-0007', '270', '270-003', 'RT 007/RW 001', NULL, NULL, '35782700030007.jpg'),
('270-003-0008', '270', '270-003', 'RT 008/RW 001', NULL, NULL, '35782700030008.jpg'),
('270-003-0009', '270', '270-003', 'RT 009/RW 001', NULL, NULL, '35782700030009.jpg'),
('270-003-0010', '270', '270-003', 'RT 001/RW 002', NULL, NULL, '35782700030010.jpg'),
('270-003-0011', '270', '270-003', 'RT 002/RW 002', NULL, NULL, '35782700030011.jpg'),
('280-004-0001', '280', '280-004', 'RT 001/RW 001', NULL, NULL, '35782800040001.jpg'),
('280-004-0002', '280', '280-004', 'RT 002/RW 001', NULL, NULL, '35782800040002.jpg'),
('280-004-0003', '280', '280-004', 'RT 003/RW 001', NULL, NULL, '35782800040003.jpg'),
('280-004-0004', '280', '280-004', 'RT 004/RW 001', NULL, NULL, '35782800040004.jpg'),
('280-004-0005', '280', '280-004', 'RT 005/RW 001', NULL, NULL, '35782800040005.jpg'),
('280-004-0006', '280', '280-004', 'RT 006/RW 001', NULL, NULL, '35782800040006.jpg'),
('280-004-0007', '280', '280-004', 'RT 007/RW 001', NULL, NULL, '35782800040007.jpg'),
('280-004-0008', '280', '280-004', 'RT 008/RW 001', NULL, NULL, '35782800040008.jpg'),
('280-004-0009', '280', '280-004', 'RT 009/RW 001', NULL, NULL, '35782800040009.jpg'),
('280-004-0010', '280', '280-004', 'RT 010/RW 001', NULL, NULL, '35782800040010.jpg'),
('280-004-0011', '280', '280-004', 'RT 011/RW 011', NULL, NULL, '35782800040011.jpg'),
('280-004-0012', '280', '280-004', 'RT 012/RW 001', NULL, NULL, '35782800040012.jpg'),
('280-004-0013', '280', '280-004', 'RT 001/RW 002', NULL, NULL, '35782800040013.jpg'),
('280-004-0014', '280', '280-004', 'RT 002/RW 002', NULL, NULL, '35782800040014.jpg'),
('280-004-0015', '280', '280-004', 'RT 003/RW 002', NULL, NULL, '35782800040015.jpg'),
('281-001-0001', '281', '281-001', 'RT 001/RW 001', NULL, NULL, '35782810010001.jpg'),
('281-001-0002', '281', '281-001', 'RT 002/RW 001', NULL, NULL, '35782810010002.jpg'),
('281-001-0003', '281', '281-001', 'RT 003/RW 001', NULL, NULL, '35782810010003.jpg'),
('281-001-0004', '281', '281-001', 'RT 004/RW 001', NULL, NULL, '35782810010004.jpg'),
('281-001-0005', '281', '281-001', 'RT 005/RW 001', NULL, NULL, '35782810010005.jpg'),
('281-001-0006', '281', '281-001', 'RT 001/RW 002', NULL, NULL, '35782810010006.jpg'),
('281-001-0007', '281', '281-001', 'RT 002/RW 002', NULL, NULL, '35782810010007.jpg'),
('281-001-0008', '281', '281-001', 'RT 003/RW 002', NULL, NULL, '35782810010008.jpg'),
('281-001-0009', '281', '281-001', 'RT 001/RW 003', NULL, NULL, '35782810010009.jpg'),
('281-001-0010', '281', '281-001', 'RT 002/RW 003', NULL, NULL, '35782810010010.jpg'),
('281-001-0011', '281', '281-001', 'RT 003/RW 003', NULL, NULL, '35782810010011.jpg'),
('281-001-0012', '281', '281-001', 'RT 004/RW 003', NULL, NULL, '35782810010012.jpg'),
('281-001-0013', '281', '281-001', 'RT 005/RW 003', NULL, NULL, '35782810010013.jpg'),
('281-001-0014', '281', '281-001', 'RT 006/RW 003', NULL, NULL, '35782810010014.jpg'),
('281-001-0015', '281', '281-001', 'RT 007/RW 003', NULL, NULL, '35782810010015.jpg'),
('281-001-0016', '281', '281-001', 'RT 008/RW 003', NULL, NULL, '35782810010016.jpg'),
('281-001-0017', '281', '281-001', 'RT 001/RW 004', NULL, NULL, '35782810010017.jpg');

-- --------------------------------------------------------

--
-- Stand-in structure for view `tabel_kel`
-- (See below for the actual view)
--
CREATE TABLE `tabel_kel` (
`username` varchar(255)
,`nama_kec` varchar(255)
,`id_kel` varchar(255)
,`id_kec` varchar(255)
,`nama_kel` varchar(255)
,`luas_kel` decimal(6,2)
,`penduduk_kel` int(7)
,`image_kel` varchar(255)
,`jumlah_rt` int(2)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `tabel_rt`
-- (See below for the actual view)
--
CREATE TABLE `tabel_rt` (
`username` varchar(255)
,`nama_kec` varchar(255)
,`nama_kel` varchar(255)
,`id_rt` varchar(255)
,`id_kec` varchar(255)
,`id_kel` varchar(255)
,`nama_rt` varchar(255)
,`luas_rt` decimal(6,2)
,`penduduk_rt` int(7)
,`image_rt` varchar(255)
);

-- --------------------------------------------------------

--
-- Structure for view `tabel_kel`
--
DROP TABLE IF EXISTS `tabel_kel`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `tabel_kel`  AS  select `a`.`username` AS `username`,`b`.`nama_kec` AS `nama_kec`,`c`.`id_kel` AS `id_kel`,`c`.`id_kec` AS `id_kec`,`c`.`nama_kel` AS `nama_kel`,`c`.`luas_kel` AS `luas_kel`,`c`.`penduduk_kel` AS `penduduk_kel`,`c`.`image_kel` AS `image_kel`,`c`.`jumlah_rt` AS `jumlah_rt` from ((`akun` `a` join `kecamatan` `b`) join `kelurahan` `c`) where ((`a`.`username` = `b`.`username`) and (`b`.`id_kec` = `c`.`id_kec`)) ;

-- --------------------------------------------------------

--
-- Structure for view `tabel_rt`
--
DROP TABLE IF EXISTS `tabel_rt`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `tabel_rt`  AS  select `d`.`username` AS `username`,`a`.`nama_kec` AS `nama_kec`,`b`.`nama_kel` AS `nama_kel`,`c`.`id_rt` AS `id_rt`,`c`.`id_kec` AS `id_kec`,`c`.`id_kel` AS `id_kel`,`c`.`nama_rt` AS `nama_rt`,`c`.`luas_rt` AS `luas_rt`,`c`.`penduduk_rt` AS `penduduk_rt`,`c`.`image_rt` AS `image_rt` from (((`kecamatan` `a` join `kelurahan` `b`) join `rt` `c`) join `akun` `d`) where ((`a`.`id_kec` = `c`.`id_kec`) and (`b`.`id_kel` = `c`.`id_kel`) and (`d`.`username` = `a`.`username`)) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `akun`
--
ALTER TABLE `akun`
  ADD PRIMARY KEY (`id_akun`),
  ADD KEY `uname_kec` (`username`);

--
-- Indexes for table `kecamatan`
--
ALTER TABLE `kecamatan`
  ADD PRIMARY KEY (`id_kec`),
  ADD KEY `username` (`username`);

--
-- Indexes for table `kelurahan`
--
ALTER TABLE `kelurahan`
  ADD PRIMARY KEY (`id_kel`),
  ADD KEY `id_kec` (`id_kec`);

--
-- Indexes for table `rt`
--
ALTER TABLE `rt`
  ADD PRIMARY KEY (`id_rt`),
  ADD KEY `rt_ibfk_1` (`id_kel`),
  ADD KEY `id_kec` (`id_kec`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `akun`
--
ALTER TABLE `akun`
  MODIFY `id_akun` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `kecamatan`
--
ALTER TABLE `kecamatan`
  ADD CONSTRAINT `uname_kec` FOREIGN KEY (`username`) REFERENCES `akun` (`username`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `kelurahan`
--
ALTER TABLE `kelurahan`
  ADD CONSTRAINT `id_kec` FOREIGN KEY (`id_kec`) REFERENCES `kecamatan` (`id_kec`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `rt`
--
ALTER TABLE `rt`
  ADD CONSTRAINT `rt_ibfk_1` FOREIGN KEY (`id_kel`) REFERENCES `kelurahan` (`id_kel`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `rt_ibfk_2` FOREIGN KEY (`id_kec`) REFERENCES `kecamatan` (`id_kec`) ON DELETE SET NULL ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
