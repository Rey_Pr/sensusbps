<section class="content">
  <div class="box box-primary">

    <!-- content header -->
    <div class="box-header with-border">
      <h2 class="box-title"><big>Edit <?=$row->nama_kec?></big></h2>
      <div class="pull-right">
          <a href="<?=site_url('admin/kecamatan')?>" class="btn btn-warning btn-flat">
            <i class="fa fa-arrow-left"></i>
          </a>
      </div>
    </div>  
    <!-- end content header -->

    <!-- isi content -->
    <div class="box-body">
      <div class="row">
      <div class="col-md-4">
        <form action="" method="post">
          <!-- <div class="form-group <?=form_error('luas_kec') ? 'has-error' : null?>">
            <label>Luas Wilayah<small> (km2)</small></label>
              <input type="hidden" name="id_kec" value="<?=$row->id_kec?>">
              <input type="text" name="luas_kec"  value="<?=$this->input->post('luas_kec') ??  $row->luas_kec?>" class="form-control">
            <?=form_error('luas_kec')?>
          </div> -->
          <div class="form-group <?=form_error('penduduk_kec') ? 'has-error' : null?>">
            <label>Jumlah Penduduk</label>
            <input type="hidden" name="id_kec" value="<?=$row->id_kec?>">
            <input type="text" name="penduduk_kec" value="<?=$this->input->post('penduduk_kec') ??  $row->penduduk_kec?>" class="form-control">
            <?=form_error('penduduk_kec')?>
          </div>
          <div class="form-group">
            <button class="btn btn-success btn-flat" type="submit">
            <i class="fa fa-paper-plane"></i> Simpan
            </button>
            <button class="btn btn-flat" type="reset">Reset</button>
          </div>
        </form>
      </div>
    </div>
    <!-- end isi content -->
    
  </div>
</section>