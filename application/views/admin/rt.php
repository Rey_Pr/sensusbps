<section class="conten-header">
  <div class="content">
    <div class="box box-primary">

    <!-- content header -->  
    <div class="box-header with-border">
      <h2 class="box-title"><big>Daftar RT di Surabaya</big></h2>
    </div>
    <!-- end content header -->

    <!-- isi content -->
    <div class="box-body table-responsive">
      <table id="example1" class="table table-striped table-bordered table-hover">
        <thead>
          <tr>
            <th class="text-center">ID.</th>
            <th class="text-center">Nama Kecamatan</th>
            <th class="text-center">Nama Kelurahan</th>
            <th class="text-center">RT</th>
            <th class="text-center">Luas Wilayah (km2)</th>
            <th class="text-center">Penduduk</th>
            <th class="text-center">Aksi</th>
          </tr>
        </thead>
        <tbody>
          <?php
          foreach($row -> result() as $key => $data) : ?>
            <tr>
              <td style="text-align: center;"><?=$data->id_rt?></td>
              <td><?=$data->nama_kec?></td>
              <td><?=$data->nama_kel?></td>
              <td><?=$data->nama_rt?></td>
              <td style="text-align: right;"><?=$data->luas_rt?></td>
              <td style="text-align: right;"><?=number_format(($data->penduduk_rt),0,',','.')?></td>
              <td style="text-align: right;"> 
                <a href="<?=site_url('admin/rt/edit/'.$data->id_rt)?>" class="btn btn-primary btn-xs">
                  <i class="fa fa-pencil"></i> Ubah
                </a>
                <a href="<?=site_url('admin/rt/detail_rt/'.$data->id_rt)?>" class="btn btn-primary btn-xs bg-olive">
                  <i class="fa fa-eye"></i> Lihat
                </a>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
    <!-- end isi content -->
    
  </div>
</section>