<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	// fungsi login
	public function login() {
		$this->load->view('login');
	}

	// proses login
	public function process() {
		// ambil username dan password dari inputan
		$post = $this->input->post(null, TRUE);
		if (isset($post['login'])) {
			$this->load->model('Admin_m');
			$query = $this->Admin_m->login($post);
			if($query->num_rows() > 0){
				$row = $query->row();
				$params = array(
					'username' => $row ->username,
					'status' => $row ->status
				);
				$this->session->set_userdata($params); // mengarahkan login sesuai user
				if ($this->session->userdata('status') == 1){
				echo "<script>
					alert('Selamat, login berhasil');
					window.location='".site_url('admin/dashboard')."'
				</script>";
				}
				else{
					echo "<script>
					alert('Selamat, login berhasil');
					window.location='".site_url('user/dashboard')."'
				</script>";
				}
			}
			else{
				echo "<script>
					alert('Login gagal, Username/Password Salah');
					window.location='".site_url('auth/login')."'
				</script>";
			}
		}
	}

	// fungsi logout
	public function logout() {
		$params = array('username', 'status');
		$this->session->unset_userdata($params);
		redirect('auth/login');
	}

}