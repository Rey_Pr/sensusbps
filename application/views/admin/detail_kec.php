<section class="content">
  <div class="box box-primary">

    <!-- content header -->
    <div class="box-header with-border">
      <h2 class="box-title"><big>Detail <?=$row->nama_kec?></big></h2>
      <div class="pull-right">
        <a href="<?=site_url('admin/kecamatan')?>" class="btn btn-warning btn-flat">
          <i class="fa fa-arrow-left"></i>
        </a>
      </div>
    </div>
    <!-- end content header -->

    <!-- isi content -->
    <div class="box-body">
      <div class="row">
        <div class="col-md-6">
          <input type="hidden" name="id_kec" value="<?=$row->id_kec?>">
          <div class="form-group">
            <label><h4>Jumlah Kelurahan: </h4></label><br>
            <label><big><?=$row->jumlah_kel?></big></label><br>
          </div>
          <div class="form-group">
            <label><h4>Luas Wilayah: </h4></label><br>
            <label><big><?=$row->luas_kec?> km2</big></label><br>
          </div>
          <div class="form-group">
            <label><h4>Jumlah Penduduk</h4></label><br>
            <label><big><?=number_format(($row->penduduk_kec),0,',','.')?> jiwa</big></label><br>
          </div>
        </div>
        <div class="col-md-6">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Peta Wilayah</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
              <div class="chart">
                <center><img class="img-responsive" width="400" height="400" src="<?php echo base_url().'assets/images/Kec/'.$row->image_kec;?>" alt=""></center>
              </div>
            </div>
        </div>
      </div>
    </div>
    <!-- end isi content -->

  </div>
  <div class="box box-primary">
    
  </div> 
</section>