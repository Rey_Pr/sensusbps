<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelurahan extends CI_Controller {

	function __construct() {
		parent::__construct();

		// load model dan library
		$this->load->model('Pengguna_m');
		$this->load->model('Admin_m');
		$this->load->library('form_validation');

		// cek status login
		if ($this->Admin_m->is_role() != "2"){
            redirect("auth/login");
        }
	}

	// ambil data dari model dan ditampilkan ke view
	public function index() {
		$data["row"] = $this->Pengguna_m->getKel();
		$this->template->load('template', 'user/kelurahan', $data);
	}

	// ambil detail kelurahan dan ditampilkan ke view
	public function detail_kel($id) {
        if ($this->form_validation->run()==FALSE){
			$query = $this->Pengguna_m->getDetailKel($id);
			if($query->num_rows() > 0){
				$data['row'] = $query -> row();
				$this->template->load('template', 'user/detail_kel', $data);
			}
			else{
				echo "<script>alert('Data tidak ditemukan');";
				echo "window.location='".site_url('user/kelurahan')."';</script>";
			}
		}
	}

}