<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RT_m extends CI_Model{

	// ambil data dari tabel rt
	public function getDetailRT($id=null) {
		$this->db->from('tabel_rt');
		if($id != null){
			$this->db->where('id_rt', $id);
		}
		$query = $this->db->get();
		return $query;
	}

	public function edit($post)
	{
		// $params['luas_rt'] = $post['luas_rt'];
		$params['penduduk_rt'] = $post['penduduk_rt'];
		// if(!empty($post['password'])){
		// 	$params['password'] = md5($post['password']);
		// }
		// $params['status'] = $post['status'];
		$this->db->where('id_rt', $post['id_rt']);
		$this->db->update('rt', $params);
	}

	// ambil gambar rt
    public function getGbrRt($id=null)
    {
        $sesi = $this->session->userdata("username");
        $this->db->select('image_rt');
        $this->db->from('tabel_rt');
        $this->db->where('username = ', $sesi);
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            return $query->row()->image_rt;
        }
        return false;
    }
    
	// public function delete($id)
	// {
	// 	$this->db->where('id_akun', $id);
	// 	$this->db->delete('akun');
	// }

	// public function getKec($id=null)
	// {
	// 	$this->db->from('kecamatan');
	// 	if($id != null){
	// 		$this->db->where('id_kec', $id);
	// 	}
	// 	$query = $this->db->get();
	// 	return $query;
	// }
}