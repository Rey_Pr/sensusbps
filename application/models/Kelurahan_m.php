<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelurahan_m extends CI_Model{

	// ambil data dari tabel kelurahan
	public function getDetailKel($id=null) {
        $this->db->from('tabel_kel');
        if($id != null){
            $this->db->where('id_kel', $id);
        }
        $query = $this->db->get();
        return $query;
    }
	
	// //SELECT a.luas_kel FROM tabel_kel a JOIN kecamatan b WHERE a.id_kel = "010-001" AND a.id_kec = b.id_kec
	// public function getLuas($id=null)
	// // {
	// // 	$sesi = $this->session->userdata("username");
 // //        $this->db->select('luas_kel');
 // //        $this->db->from('tabel_kel');
 // //        $this->db->where('id_kel = ', $id);
 // //        return $this->db->get();
	// // }

	// ambil data luas
	public function getLuas($id=null) {
	    $this->db->select('luas_kel')->from('tabel_kel')->where('id_kel',$id);
	    $query = $this->db->get();
	    if ($query->num_rows() > 0) {
	         return $query->row()->luas_kel;
	    }
    	return false;
	}

	// ambil data jumlah penduduk
	public function getPenduduk($id=null) {
		$sesi = $this->session->userdata("username");
        $this->db->select('penduduk_kel');
        $this->db->from('tabel_kel');
        $this->db->where('id_kel = ', $id);
        return $this->db->get();
	}

	// edit data
	public function edit($post)
	{
		// $params['luas_kel'] = $post['luas_kel'];
		$params['penduduk_kel'] = $post['penduduk_kel'];
		$this->db->where('id_kel', $post['id_kel']);
		$this->db->update('kelurahan', $params);
	}

}