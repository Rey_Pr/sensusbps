<section class="conten-header">
  <div class="content">
    <div class="box box-primary">

    <!-- content header -->
    <div class="box-header with-border">
        <h2 class="box-title"><big>Daftar Akun Pengguna</big></h2>
        <div class="pull-right">
          <a href="<?=site_url('admin/akun/add')?>" class="btn btn-primary btn-flat">
            <i class="fa fa-user-plus"></i> Tambah
          </a>
        </div>
    </div>
    <!-- end content header -->

    <!-- isi content -->
    <div class="box-body table-responsive">
      <table id="example1" class="table table-bordered table-striped table-responsive">
        <thead>
          <tr>
            <th style="text-align: center;">No.</th>
            <th style="text-align: center;">Username</th>
            <th style="text-align: center;">Password</th>
            <th style="text-align: center;">Kecamatan</th>
            <th class="text-center">Aksi</th>
          </tr>
        </thead>
        <tbody>
          <?php $no = 1;
          foreach($row -> result() as $key => $data)
          {?>
          <tr>
            <td width="50px" style="text-align: center;"><?=$no++;?></td>
            <td><?=$data->username?></td>
            <td><?=($data->password)?></td>
            <td><?=$data->nama?></td>
            <td class="text-center" width="160px">
              <form action="<?=site_url('admin/akun/del/'.$data->username)?>" method="post">
                <a href="<?=site_url('admin/akun/edit/'.$data->username)?>" class="btn btn-primary btn-xs">
                  <i class="fa fa-pencil"></i> Ubah
                </a>
                <input type="hidden" name="username" value="<?=$data->username?>">
                <button onclick="return confirm('Apakah Anda Yakin?')" class="btn btn-danger btn-xs">
                  <i class="fa fa-trash"></i> Hapus
                </button>
              </form>
            </td>
          </tr>
          <?php
          } ?>
        </tbody>
      </table>
    </div>
  </div>
  <!-- end isi content -->

</section>
