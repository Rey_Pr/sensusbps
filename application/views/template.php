<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
  <link rel="icon"  href="<?php echo base_url().'assets/images/bps.png'?>">
  <title>BPS Kota Surabaya</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- css -->
  <link rel="stylesheet" href="<?=base_url()?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition skin-blue sidebar-mini">

  <!-- header -->
  <div class="wrapper">
    <header class="main-header">
      <!-- link ke landing berdasarkan status -->
      <?php if($this->session->userdata('status') == 1){ ?>
      <a href="<?=base_url().'admin/dashboard'?>" class="logo">
        <span class="logo-mini"><b>BPS</b></span>
        <span class="logo-lg"><b>BPS</b>Kota Surabaya</span>
      </a>
      <?php } ?>
      <?php if($this->session->userdata('status') == 2){ ?>
      <a href="<?=base_url().'user/dashboard'?>" class="logo">
        <span class="logo-mini"><b>BPS</b></span>
        <span class="logo-lg"><b>BPS</b>Kota Surabaya</span>
      </a>
      <?php } ?>
      <nav class="navbar navbar-static-top">
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>

        <!-- menu profil -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="<?=base_url()?>assets/dist/img/user2-160x160.jpg" class="user-image">
                <span class="hidden-xs"><?=$this->fungsi->user_login()->username?></span>
              </a>
              <ul class="dropdown-menu">
                <li class="user-header">
                  <img src="<?=base_url()?>assets/dist/img/user2-160x160.jpg" class="img-circle">
                  <p>
                    <?=$this->fungsi->user_login()->nama?>
                  </p>
                </li>
                <li class="user-footer">
                  <div class="pull-right">
                    <a href="<?=site_url('auth/logout')?>" class="btn btn-flat bg-red">Keluar</a>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>
        <!-- end menu profil -->
      </nav>
    </header>
    <!-- end header -->

  <!-- Left side column -->
  <aside class="main-sidebar">
    <section class="sidebar">
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?=base_url()?>assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><h4><?=$this->fungsi->user_login()->nama?></h4></p>
        </div>
      </div>
      <!-- sidebar menu-->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <!-- side menu berdasarkan status -->
        <?php if($this->session->userdata('status') == 1){ ?>
        <li>
          <a href="<?=base_url().'admin/kecamatan'?>"><i class="fa fa-location-arrow"></i> <span>Kecamatan</span></a>
        </li>
        <li>
          <a href="<?=base_url().'admin/kelurahan'?>"><i class="fa fa-map-marker"></i> <span>Kelurahan</span></a>
        </li>
        <li>
          <a href="<?=base_url().'admin/rt'?>"><i class="fa fa-users"></i> <span>RT</span></a>
        </li>
        <li>
          <a href="<?=base_url().'admin/akun'?>"><i class="fa fa-user"></i> <span>Akun</span></a>
        </li>
        <?php } ?>
        <?php if($this->session->userdata('status') == 2){ ?>
        <li>
          <a href="<?=base_url().'user/kelurahan'?>"><i class="fa fa-map-marker"></i> <span>Kelurahan</span></a>
        </li>
        <li>
          <a href="<?=base_url().'user/rt'?>"><i class="fa fa-users"></i> <span>RT</span></a>
        </li>
        <?php } ?>
      </ul>  
    </section>
  </aside>

  <!-- Content Wrapper -->
  <div class="content-wrapper">
      <?php echo $contents ?>
  </div>
  <!-- end content wrapper -->

  <!-- footer -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; 2020 <a href="https://surabayakota.bps.go.id">BPS Kota Surabaya</a>.</strong> All rights
    reserved.
  </footer>
  <!-- end footer -->

</div>

<!-- javascript -->
<script src="<?=base_url()?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?=base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?=base_url()?>assets/bower_components/fastclick/lib/fastclick.js"></script>
<script src="<?=base_url()?>assets/dist/js/adminlte.min.js"></script>
<script src="<?=base_url()?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?=base_url()?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?=base_url()?>assets/bower_components/fastclick/lib/fastclick.js"></script>
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>