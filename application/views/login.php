<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="icon" type="image/png" href="<?php echo base_url().'assets/images/bps.png'?>">
  <title>Blok Sensus | Masuk</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- css -->
  <link rel="stylesheet" href="<?=base_url()?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition login-page" style="background-color: #087097">
  <div class="login-box">

    <!-- login logo -->
    <div class="login-logo">
      <a href="<?=base_url('auth/login')?>"><img src="<?=base_url()?>assets/dist/img/LogoBPS.png" class="logo"></a>
    </div>
    <!-- login logo -->

    <!-- input login -->
    <div class="login-box-body">
      <p class="login-box-msg">Masukkan Username dan Password</p>
      <form action="<?=site_url('auth/process')?>" method="post">
        <div class="form-group has-feedback">
          <input type="text" name="username" class="form-control" placeholder="Username" required autofocus="">
          <span class="glyphicon glyphicon-user form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
          <input type="password" name="password" class="form-control" placeholder="Password" required="">
          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="row">
          <div class="col-xs-4" style="float: right;">
            <button type="submit" name="login" class="btn btn-primary btn-block btn-flat">Masuk</button>
          </div>
        </div>
      </form>
    </div>
    <!-- end input login -->
    
  </div>
</body>
</html>