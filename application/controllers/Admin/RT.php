<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RT extends CI_Controller {

	function __construct() {
		parent::__construct();

		// load model dan library
		$this->load->model('RT_m');
		$this->load->model('Admin_m');
		$this->load->library('form_validation');

		// cek status login
		if($this->Admin_m->is_role() != "1"){
            redirect("auth/login");
        }
	}

	// mengambil data dari model dan ditampilkan ke view
	public function index() {
		$data['row'] = $this->RT_m->getDetailRT();
		$this->template->load('template', 'admin/rt', $data);
	}

	// ambil detail rt dan ditampilkan ke view
	public function detail_rt($id){
		$data["gambar"] = $this->RT_m->getGbrRt();
        if ($this->form_validation->run()==FALSE){
			$query = $this->RT_m->getDetailRT($id);
			if($query->num_rows() > 0){
				$data['row'] = $query -> row();
				$this->template->load('template', 'admin/detail_rt', $data);
			}
			else{
				echo "<script>alert('Data tidak ditemukan');";
				echo "window.location='".site_url('admin/rt')."';</script>";
			}
		}
	}

	public function edit($id)
	{
		// $this->form_validation->set_rules('luas_rt','luas_rt','required|decimal');
		$this->form_validation->set_rules('penduduk_rt','penduduk_rt','required|numeric');
		$this->form_validation->set_message('required', '%s Masih Kosong, Silahkan isi');
		// $this->form_validation->set_message('decimal', '%s Harus Diisi dengan Angka Desimal');
		$this->form_validation->set_message('numeric', '%s Harus Diisi dengan Angka');

		$this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

		if ($this->form_validation->run()==FALSE){
			$query = $this->RT_m->getDetailRT($id);
			if($query->num_rows() > 0){
				$data['row'] = $query -> row();
				$this->template->load('template', 'admin/editrt', $data);
			} else {
				echo "<script>alert('Data tidak ditemukan');";
				echo "window.location='".site_url('Admin/rt')."';</script>";
			}
		}else{
			$post = $this->input->post(null, TRUE);
			$this->RT_m->edit($post);
			if($this->db->affected_rows() > 0){
				echo "<script>alert('Data berhasil diubah');</script>";
			}
			echo "<script>window.location='".site_url('Admin/rt')."';</script>";
		}
	}


}