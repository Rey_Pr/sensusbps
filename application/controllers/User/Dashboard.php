<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	
	function __construct() {
		parent::__construct();

		// load model
		$this->load->model('Pengguna_m');
		$this->load->model('Admin_m');

		// cek status login
		if($this->Admin_m->is_role() != "2"){
            redirect("auth/login");
        }
	}

	// ambil data dari model dan ditampilkan ke view
	public function index() {
		$data["jumlah_kel"] = $this->Pengguna_m->getJumlahKel();
		$data["jumlah_rt"] = $this->Pengguna_m->getJumlahRT();
        $data["luas"] = $this->Pengguna_m->getLuas();
        $data["penduduk"] = $this->Pengguna_m->getPenduduk();
        $data["gambar"] = $this->Pengguna_m->getGbrKec();
		$this->template->load('template', 'user/dashboard', $data);
	}
	
}