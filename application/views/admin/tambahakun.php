<section class="content">
  <div class="box">

    <!-- content header -->
    <div class="box-header">
        <h2 class="box-title"><big>Tambah Akun</big></h2>
        <div class="pull-right">
          <a href="<?=site_url('admin/akun')?>" class="btn btn-warning btn-flat">
            <i class="fa fa-undo"></i> Kembali
          </a>
        </div>
    </div>
    <!-- end content header -->

    <!-- isi content -->
    <div class="box-body">
        <div class="row">
        	<div class="col-md-4">
    		<!-- <?php echo validation_errors();?> -->
        		<form action="" method="post">
        			<div class="form-group <?=form_error('nama') ? 'has-error' : null?>">
        				<label>Nama Kecamatan *</label>
        				<input type="text" name="nama"  value="<?=set_value('nama')?>" class="form-control" style="width: 500px;">
        				<?=form_error('nama')?>
        			</div>
        			<div class="form-group <?=form_error('username') ? 'has-error' : null?>">
        				<label>Username *</label>
        				<input type="text" name="username" value="<?=set_value('username')?>" class="form-control" style="width: 500px;">
        				<?=form_error('username')?>
        			</div>
        			<div class="form-group <?=form_error('password') ? 'has-error' : null?>">
        				<label>Password *</label>
        				<input type="password" name="password" class="form-control">
        				<?=form_error('password')?>
        			</div>
        			<div class="form-group <?=form_error('passconf') ? 'has-error' : null?>">
        				<label>Konfirmasi Password *</label>
        				<input type="password" name="passconf" class="form-control">
        				<?=form_error('passconf')?>
        			</div>
        			<div class="form-group <?=form_error('status') ? 'has-error' : null?>">
        				<label>Status *</label>
        				<select class="form-control" name="status">
        					<option value="">- Pilih -</option>
        					<option value="1" <?=set_value('status')==1 ? "selected" : null?>>Akun Master</option>
        					<option value="2" <?=set_value('status')==2 ? "selected" : null?>>Akun Kecamatan</option>
        				</select>
        				<?=form_error('status')?>
        			</div>
        			<div class="form-group">
        				<button class="btn btn-success btn-flat" type="submit">
        				<i class="fa fa-paper-plane"></i> Simpan
	        			</button>
	        			<button class="btn btn-flat" type="reset">Reset</button>
        			</div>
        		</form>
        	</div>
        </div>
    </div>
    <!-- end content header -->

  </div>
</section>