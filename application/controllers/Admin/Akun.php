<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Akun extends CI_Controller {

	function __construct() {
		parent::__construct();

		// load library form validation
	  	$this->load->library('form_validation');
	  	$this->form_validation->CI =& $this;

	  	// load model
		$this->load->model('Admin_m');

		// cek status login
		if($this->Admin_m->is_role() != "1"){
            redirect("auth/login");
        }
	}

	// ambil data dari model dan ditampilkan ke view
	public function index() {
		$data['row'] = $this->Admin_m->get();
		$this->template->load('template', 'admin/akun', $data);
	}

	// fungsi tambah data
	public function add() {
		$this->form_validation->set_rules('username','Username','required|is_unique[akun.username]');
		$this->form_validation->set_rules('nama','Nama','required|min_length[5]|is_unique[akun.nama]');
		$this->form_validation->set_rules('password','Password','required|min_length[5]');
		$this->form_validation->set_rules('passconf','Konfirmasi Password','required|matches[password]',
			array('matches' => '{field} tidak sesuai dengan password'));
		$this->form_validation->set_rules('status','Status','required');
		$this->form_validation->set_message('required', '%s Masih Kosong, Silahkan isi');
		$this->form_validation->set_message('min_length', '{field} Minimal 5 Karakter');
		$this->form_validation->set_message('is_unique', '{field} ini Sudah Dipakai, Silahkan Ganti yang Lain');

		$this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

		if ($this->form_validation->run()==FALSE){
			$this->template->load('template', 'admin/tambahakun');
		}
		else{
			$post = $this->input->post(null, TRUE);
			$this->Admin_m->add($post);
			if($this->db->affected_rows() > 0){
				echo "<script>alert('Data berhasil disimpan');</script>";
			}
			echo "<script>window.location='".site_url('Admin/akun')."';</script>";
		}
	}

	// fungsi cek username
	function username_check($str) {
		// $this->load->helper('security');
		$post = $this->input->post(null, TRUE);
		$query = $this->db->query(" SELECT * FROM akun WHERE username = '$post[username]' AND id_akun != '$post[id_akun]'");
		if ($query->num_rows() > 0) {
			$this->form_validation->set_message('username_check', '{field} ini Sudah Dipakai, Silahkan Ganti yang Lain');
			return FALSE;
		}
		else{
			return TRUE;
		}
	}

	// fungsi cek nama
	function name_check($str) {
		$post = $this->input->post(null, TRUE);
		$query = $this->db->query(" SELECT * FROM akun WHERE nama = '$post[nama]' AND id_akun != '$post[id_akun]'");
		if ($query->num_rows() > 0) {
			$this->form_validation->set_message('name_check', '{field} ini Sudah Dipakai, Silahkan Ganti yang Lain');
			return FALSE;
		}
		else{
			return TRUE;
		}
	}

	// fungsi edit data
	public function edit($id) {
		$this->form_validation->set_rules('nama','Nama','required|callback_name_check');
		$this->form_validation->set_rules('username','Username','required|min_length[5]|callback_username_check');
		if($this->input->post('password')){
			$this->form_validation->set_rules('password','Password','min_length[5]');
			$this->form_validation->set_rules('passconf','Konfirmasi Password','matches[password]',
				array('matches' => '{field} tidak sesuai dengan password'));
		}
		if($this->input->post('passconf')){
			$this->form_validation->set_rules('passconf','Konfirmasi Password','matches[password]',
				array('matches' => '{field} tidak sesuai dengan password'));
		}
		$this->form_validation->set_rules('status','Status','required');
		$this->form_validation->set_message('required', '%s Masih Kosong, Silahkan isi');
		$this->form_validation->set_message('min_length', '{field} Minimal 5 Karakter');
		$this->form_validation->set_message('is_unique', '{field} ini Sudah Dipakai, Silahkan Ganti yang Lain');
		
		$this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

		if ($this->form_validation->run()==FALSE){
			$query = $this->Admin_m->get($id);
			if($query->num_rows() > 0){
				$data['row'] = $query -> row();
				$this->template->load('template', 'admin/editakun', $data);
			} 
			else{
				echo "<script>alert('Data tidak ditemukan');";
				echo "window.location='".site_url('Admin/akun')."';</script>";
			}
		}
		else{
			$post = $this->input->post(null, TRUE);
			$this->Admin_m->edit($post);
			if($this->db->affected_rows() > 0){
				echo "<script>alert('Data berhasil diubah');</script>";
			}
			echo "<script>window.location='".site_url('Admin/akun')."';</script>";
		}
	}

	// fungsi hapus data
	public function del($id) {
		$id = $this->input->post('username');
		$this->Admin_m->delete($id);

		if($this->db->affected_rows() > 0){
				echo "<script>alert('Data berhasil dihapus');</script>";
		}
		echo "<script>window.location='".site_url('Admin/akun')."';</script>";
	}

	public function decrip($data)
	{
		$cryptKey = 'd8578edf8458ce06fbc5bb76a58c5ca4';
		$qDecoded = rtrim( mcrypt_decrypt( MCRYPT_RIJNDAEL_256, md5( $cryptKey), base64_decode( $data), MCRYPT_MODE_CBC, md5( md5($cryptKey) ) ) ,"\0");
		return ( $qDecoded);
		echo "<script>window.location='".site_url('Admin/akun')."';</script>";
	}
}