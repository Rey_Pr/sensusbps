<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RT extends CI_Controller {

	function __construct() {
		parent::__construct();

		// load model dan library
		$this->load->model('Pengguna_m');
		$this->load->model('Admin_m');
		$this->load->library('form_validation');

		// cek status login
		if ($this->Admin_m->is_role() != "2"){
            redirect("auth/login");
        }
	}

	// ambil data dari model dan ditampilkan ke view
	public function index() {
	 	$data["row"] = $this->Pengguna_m->getRT();
		$this->template->load('template', 'user/rt', $data);
	}

	// ambil detail rt dan ditampilkan ke view
	public function detail_rt($id) {
	 	$data["gambar"] = $this->Pengguna_m->getGbrRt();
        if ($this->form_validation->run()==FALSE){
			$query = $this->Pengguna_m->getDetailRT($id);
			if($query->num_rows() > 0){
				$data['row'] = $query -> row();
				$this->template->load('template', 'user/detail_rt', $data);
			}
			else{
				echo "<script>alert('Data tidak ditemukan');";
				echo "window.location='".site_url('user/rt')."';</script>";
			}
		}
	}

}