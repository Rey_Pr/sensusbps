<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengguna_m extends CI_Model{

    // ambil data dari tabel kelurahan
	public function getKel() {
		$sesi = $this->session->userdata("username");
        $this->db->select('*');
        $this->db->from('kecamatan');
        $this->db->where('kecamatan.id_kec = tabel_kel.id_kec');
        $this->db->where('kecamatan.username = ', $sesi);
        return $this->db->get("tabel_kel");
	}

    // ambil data jumlah kelurahan
	public function getJumlahKel() {
       $sesi = $this->session->userdata("username");
       $this->db->from('kecamatan');
       $this->db->where('kecamatan.id_kec = tabel_kel.id_kec');
       $this->db->where('kecamatan.username = ', $sesi);
       return $this->db->count_all_results("tabel_kel");
    }

    // ambil data dari tabel rt
    public function getRT() {
		$sesi = $this->session->userdata("username");
        $this->db->select('*');
        $this->db->from('kecamatan');
        $this->db->where('kecamatan.id_kec = tabel_rt.id_kec');
        $this->db->where('kecamatan.username = ', $sesi);
        return $this->db->get("tabel_rt");
	}

    // ambil data jumlah rt
    public function getJumlahRT() {
    	$sesi = $this->session->userdata("username");
    	$this->db->from('kecamatan');
    	$this->db->where('kecamatan.id_kec = tabel_rt.id_kec');
    	$this->db->where('kecamatan.username = ', $sesi);
    	return $this->db->count_all_results("tabel_rt");
    }

    // ambil gambar kecamatan
    public function getGbrKec($id=null)
    {
        $sesi = $this->session->userdata("username");
        $this->db->select('image_kec');
        $this->db->from('kecamatan');
        $this->db->where('username = ', $sesi);
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            return $query->row()->image_kec;
        }
        return false;
    }

    // ambil gambar kelurahan
    public function getGbrKel($id=null)
    {
        $sesi = $this->session->userdata("username");
        $this->db->select('image_kel');
        $this->db->from('tabel_kel');
        $this->db->where('username = ', $sesi);
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            return $query->row()->image_kel;
        }
        return false;
    }

    // ambil gambar rt
    public function getGbrRt($id=null)
    {
        $sesi = $this->session->userdata("username");
        $this->db->select('image_rt');
        $this->db->from('tabel_rt');
        $this->db->where('username = ', $sesi);
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            return $query->row()->image_rt;
        }
        return false;
    }

    // ambil data luas kecamatan
    public function getLuas() {
        $sesi = $this->session->userdata("username");
        $this->db->select_sum('luas_kel');
        $this->db->from('kecamatan');
        $this->db->where('kecamatan.id_kec = tabel_kel.id_kec');
        $this->db->where('kecamatan.username = ', $sesi);
        $hasil = $this->db->get("tabel_kel")->row();
        return $hasil->luas_kel;
    }

    // ambil data jumlah penduduk kecamatan
    public function getPenduduk() {
        $sesi = $this->session->userdata("username");
        $this->db->select_sum('penduduk_kel');
        $this->db->from('kecamatan');
        $this->db->where('kecamatan.id_kec = tabel_kel.id_kec');
        $this->db->where('kecamatan.username = ', $sesi);
        $hasil = $this->db->get("tabel_kel")->row();
        return $hasil->penduduk_kel;
    }

    // ambil detail tabel kelurahan
    public function getDetailKel($id=null) {
        $this->db->from('tabel_kel');
        if($id != null){
            $this->db->where('id_kel', $id);
        }
        $query = $this->db->get();
        return $query;
    }

    // ambil detail tabel rt
    public function getDetailRT($id=null) {
        $this->db->from('tabel_rt');
        if($id != null){
            $this->db->where('id_rt', $id);
        }
        $query = $this->db->get();
        return $query;
    }
    
}