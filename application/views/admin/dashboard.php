<section class="content">
  <!-- dashboard detail -->
  <div class="row">

    <!-- detail kec -->
    <div class="col-lg-3 col-xs-6">
      <div class="small-box bg-aqua">
        <div class="inner">
          <h3><?php echo $jumlah_kec;?></h3>
          <p>Kecamatan</p>
        </div>
        <div class="icon">
          <i class="fa fa-location-arrow"></i>
        </div>
        <a href="<?=base_url().'admin/kecamatan'?>" class="small-box-footer">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>

    <!-- detail kel -->
    <div class="col-lg-3 col-xs-6">
      <div class="small-box bg-green">
        <div class="inner">
          <h3><?php echo $jumlah_kel;?></h3>
          <p>Kelurahan</p>
        </div>
        <div class="icon">
          <i class="fa fa-map-marker"></i>
        </div>
        <a href="<?=base_url().'admin/kelurahan'?>" class="small-box-footer">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>

    <!-- detail luas -->
    <div class="col-lg-3 col-xs-6">
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3><?php echo $luas;?><sup style="font-size: 12px">km2</sup></h3>
          <p>Luas</p>
        </div>
        <div class="icon">
          <i class="fa fa-map"></i>
        </div>
        <div class="small-box-footer" style="height: 26px;"></div>
      </div>
    </div>

    <!-- detail kec -->
    <div class="col-lg-3 col-xs-6">
      <div class="small-box bg-red">
        <div class="inner">
          <h3 style="font-size: 37px"><?=number_format(($penduduk),0,',','.')?><sup style="font-size: 12px">jiwa</sup></h3>
          <p>Penduduk</p>
        </div>
        <div class="icon">
          <i class="fa fa-child"></i>
        </div>
        <div class="small-box-footer" style="height: 26px;"></div>
      </div>
    </div>

  </div>
  <!-- end dashboard detail -->

  <!-- konten -->
  <div class="box box-primary">
   <div class="box-header with-border">
    <h3 class="box-title">Peta Wilayah</h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
        </button>
      </div>
    </div>
    <div class="box-body">
      <div class="chart">
        <center><img class="img-responsive" width="700" height="500" src="<?php echo base_url().'assets/images/Surabaya.gif'?>" alt=""> </center>
      </div>
    </div>
  </div>
  <!-- end konten -->

</section>