<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_m extends CI_Model{

	// fungsi login
	public function login($post) {
		$this->db->select('*');
		$this->db->from('akun');
		$this->db->where('username', $post['username']);
		$this->db->where('password', md5($post['password']));
		$query = $this->db->get();
		return $query;
	}

	// ambil data dari tabel akun
	public function get($id=null) {
		$this->db->from('akun');
		if($id != null){
			$this->db->where('username', $id);
		}
		$query = $this->db->get();
		return $query;
	}

	// cek role
	public function is_role() {
        return $this->session->userdata('status');
    }

    // tambah data akun
	public function add($post) {
		$params['nama'] = $post['nama'];
		$params['username'] = $post['username'];
		$params['password'] = md5($post['password']);
		$params['status'] = $post['status'];
		$this->db->insert('akun', $params);
	}

	// edit data akun
	public function edit($post) {
		$params['nama'] = $post['nama'];
		$params['username'] = $post['username'];
		if(!empty($post['password'])){
			$params['password'] = md5($post['password']);
		}
		$params['status'] = $post['status'];
		$this->db->where('username', $post['username']);
		$this->db->update('akun', $params);
	}

	// hapus data akun
	public function delete($id) {
		$this->db->where('username', $id);
		$this->db->delete('akun');
	}

	// ambil data jumlah kecamatan
	public function getJumlahKec() {
        return $this->db->count_all_results("kecamatan");
    }

    // ambil data jumlah kelurahan
    public function getJumlahKel() {
        return $this->db->count_all_results("kelurahan");
    }

    // ambil data luas
    public function getLuas() {
        $this->db->select_sum('luas_kec');
        $hasil = $this->db->get("kecamatan")->row();
        return $hasil->luas_kec;
    }

    // ambil data jumlah penduduk
    public function getPenduduk() {
        $this->db->select_sum('penduduk_kec');
        $hasil = $this->db->get("kecamatan")->row();
        return $hasil->penduduk_kec;
    }
    
}