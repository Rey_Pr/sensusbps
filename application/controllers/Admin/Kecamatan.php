<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kecamatan extends CI_Controller {

	function __construct() {
		parent::__construct();

		// load model dan library
		$this->load->model('Admin_m');
		$this->load->model('Kecamatan_m');
		$this->load->library('form_validation');

		// cek status login
		if($this->Admin_m->is_role() != "1"){
            redirect("auth/login");
        }
	}

	// mengambil data dari model dan ditampilkan ke view
	public function index() {
		$data['row'] = $this->Kecamatan_m->getDetailKec();
		$this->template->load('template', 'admin/kecamatan', $data);
	}

	// ambil detail dari kecamatan
	public function detail_kec($id) {
        if ($this->form_validation->run()==FALSE){
			$query = $this->Kecamatan_m->getDetailKec($id);
			if($query->num_rows() > 0){
				$data['row'] = $query -> row();
				$this->template->load('template', 'admin/detail_kec', $data);
			} 
			else{
				echo "<script>alert('Data tidak ditemukan');";
				echo "window.location='".site_url('admin/kecamatan')."';</script>";
			}
		}
	}

	// edit data kecamatan
	public function edit($id)
	{
		// $this->form_validation->set_rules('luas_kec','luas_kec','required|decimal');
		$this->form_validation->set_rules('penduduk_kec','penduduk_kec','required|numeric');
		$this->form_validation->set_message('required', '%s Masih Kosong, Silahkan isi');
		// $this->form_validation->set_message('decimal', '%s Harus Diisi dengan Angka Desimal');
		$this->form_validation->set_message('numeric', '%s Harus Diisi dengan Angka');

		$this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

		if ($this->form_validation->run()==FALSE){
			$query = $this->Kecamatan_m->getDetailKec($id);
			if($query->num_rows() > 0){
				$data['row'] = $query -> row();
				$this->template->load('template', 'admin/editkec', $data);
			}
			else{
				echo "<script>alert('Data tidak ditemukan');";
				echo "window.location='".site_url('Admin/kecamatan')."';</script>";
			}
		}
		else{
			$post = $this->input->post(null, TRUE);
			$this->Kecamatan_m->edit($post);
			if($this->db->affected_rows() > 0){
				echo "<script>alert('Data berhasil diubah');</script>";
			}
			echo "<script>window.location='".site_url('Admin/kecamatan')."';</script>";
		}
	}
}