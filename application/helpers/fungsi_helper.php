<?php

function check_already_login(){
	$ci =& get_instance();
	$user_session = $ci->session->userdata('status');
	if($user_session == '1'){
		redirect('admin/dashboard');
	}
	else{
		redirect('user/dashboard');
	}
}

// function check_already_login2(){
// 	$ci =& get_instance();
// 	$user_session = $ci->session->userdata('username');
// 	if($user_session){
// 		redirect('admin/dashboard');
// 	}
// }

function check_not_login(){
	$ci =& get_instance();
	$user_session = $ci->session->userdata('username');
	if(!$user_session){
		redirect('auth/login');
	}
}