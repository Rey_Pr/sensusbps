<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelurahan extends CI_Controller {

	function __construct() {
		parent::__construct();
		
		// load model dan library
		$this->load->model('Kelurahan_m');
		$this->load->model('Admin_m');
		$this->load->library('form_validation');

		// cek status login
		if($this->Admin_m->is_role() != "1"){
            redirect("auth/login");
        }
	}

	// ambil data dari model dan ditampilkan ke view
	public function index()	{
		$data['row'] = $this->Kelurahan_m->getDetailKel();
		$this->template->load('template', 'admin/kelurahan', $data);
	}

	// ambil detail dari kelurahan
	public function detail_kel($id) {
        if ($this->form_validation->run()==FALSE){
			$query = $this->Kelurahan_m->getDetailKel($id);
			if($query->num_rows() > 0){
				$data['row'] = $query -> row();
				$this->template->load('template', 'admin/detail_kel', $data);
			} 
			else{
				echo "<script>alert('Data tidak ditemukan');";
				echo "window.location='".site_url('admin/kelurahan')."';</script>";
			}
		}
	}

	public function edit($id)
	{
		// $this->form_validation->set_rules('luas_kel','luas_kel','required|decimal');
		$this->form_validation->set_rules('penduduk_kel','penduduk_kel','required|numeric');
		$this->form_validation->set_message('required', '%s Masih Kosong, Silahkan isi');
		// $this->form_validation->set_message('decimal', '%s Harus Diisi dengan Angka Desimal');
		$this->form_validation->set_message('numeric', '%s Harus Diisi dengan Angka');

		$this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

		if ($this->form_validation->run()==FALSE){
			$query = $this->Kelurahan_m->getDetailKel($id);
			if($query->num_rows() > 0){
				$data['row'] = $query -> row();
				$this->template->load('template', 'admin/editkel', $data);
			}
			else {
				echo "<script>alert('Data tidak ditemukan');";
				echo "window.location='".site_url('Admin/kelurahan')."';</script>";
			}
		}
		else{
			$post = $this->input->post(null, TRUE);
			$this->Kelurahan_m->edit($post);
			if($this->db->affected_rows() > 0){
				echo "<script>alert('Data berhasil diubah');</script>";
			}
			echo "<script>window.location='".site_url('Admin/kelurahan')."';</script>";
		}
	}


}