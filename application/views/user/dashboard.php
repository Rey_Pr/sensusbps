<section class="content">
  <!-- dashboard detail -->
  <div class="row">

    <!-- detail kel -->
    <div class="col-lg-3 col-xs-6">
      <div class="small-box bg-aqua">
        <div class="inner">
          <h3><?php echo $jumlah_kel;?></h3>
          <p>Kelurahan</p>
        </div>
        <div class="icon">
          <i class="fa fa-map-marker"></i>
        </div>
        <a href="<?=base_url().'user/kelurahan'?>" class="small-box-footer">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>

    <!-- dashboard rt -->
    <div class="col-lg-3 col-xs-6">
      <div class="small-box bg-green">
        <div class="inner">
          <h3><?php echo $jumlah_rt;?></h3>
          <p>RT</p>
        </div>
        <div class="icon">
          <i class="fa fa-users"></i>
        </div>
        <a href="<?=base_url().'user/rt'?>" class="small-box-footer">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    
    <!-- dashboard luas -->  
    <div class="col-lg-3 col-xs-6">
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3><?php echo $luas;?><sup style="font-size: 12px">km2</sup></h3>
          <p>Luas</p>
        </div>
        <div class="icon">
          <i class="fa fa-map"></i>
        </div>
        <div class="small-box-footer" style="height: 26px;"></div>
      </div>
    </div>
    
    <!-- dashboard penduduk -->
    <div class="col-lg-3 col-xs-6">
      <div class="small-box bg-red">
        <div class="inner">
          <h3 style="font-size: 37px"><?=number_format(($penduduk),0,',','.')?><sup style="font-size: 12px">jiwa</sup></h3>
          <p>Penduduk</p>
        </div>
        <div class="icon">
          <i class="fa fa-child"></i>
        </div>
        <div class="small-box-footer" style="height: 26px;"></div>
      </div>
    </div>

  </div>
  <!-- end dashboard detail -->

  <!-- konten -->
  <div class="box box-primary box-responsive">
    <div class="box-header with-border">
      <h3 class="box-title">Peta Wilayah</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <div class="box-body">
        <div class="chart">
          <center><img width="500" height="300" src="<?php echo base_url().'assets/images/Kec/'.$gambar;?>" alt=""> </center>
        </div>
      </div>
    </div>
  
  <!-- end konten -->

</section>