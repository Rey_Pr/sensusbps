<section class="content">
  <div class="box box-primary">

    <!-- content header -->
    <div class="box-header with-border">
      <h2 class="box-title"><big>Detail <?=$row->nama_kel?></big></h2>
      <div class="pull-right">
        <a href="<?=site_url('admin/kelurahan')?>" class="btn btn-warning btn-flat">
          <i class="fa fa-arrow-left"></i>
        </a>
      </div>
    </div>
    <!-- end content header -->

    <!-- isi content -->
    <div class="box-body">
      <div class="row">
        <div class="col-md-6">
          <input type="hidden" name="id_kel" value="<?=$row->id_kel?>">
          <div class="form-group">
            <label><h4>Kecamatan: </h4></label><br>
            <label><big><?=$row->nama_kec?></big></label><br>
          </div>
          <div class="form-group">
            <label><h4>Jumlah RT: </h4></label><br>
            <label><big> 40<?=$row->jumlah_rt?></big></label><br>
          </div>
          <div class="form-group">
            <label><h4>Luas Wilayah: </h4></label><br>
            <label><big><?=$row->luas_kel?> km2</big></label><br>
          </div>
          <div class="form-group">
            <label><h4>Jumlah Penduduk</h4></label><br>
            <label><big><?=number_format(($row->penduduk_kel),0,',','.')?> jiwa</big></label><br>
          </div>
        </div>
        <div class="col-md-6">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Peta Wilayah</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
              <div class="chart">
                <center>
                  <a class="MagicZoom" href="<?php echo base_url().'assets/images/Kel/'.$row->id_kec.'/'.$row->image_kel;?>" rel="zoom-id:zoom;opacity-reverse:true;"> 
                    <img class="img-responsive" width="400" height="400" src="<?php echo base_url().'assets/images/Kel/'.$row->id_kec.'/'.$row->image_kel;?>" alt="">
                  </a>
                </center>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- end isi content -->
 
  </div>
</section>