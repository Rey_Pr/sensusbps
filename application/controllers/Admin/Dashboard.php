<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct() {
		parent::__construct();

		// load model
		$this->load->model('Admin_m');

		// cek status login
		if($this->Admin_m->is_role() != "1"){
            redirect("auth/login");
        }
	}

	// ambil data dari model dan ditampilkan ke view
	public function index() {
		$data["jumlah_kec"] = $this->Admin_m->getJumlahKec();
		$data["jumlah_kel"] = $this->Admin_m->getJumlahKel();
        $data["luas"] = $this->Admin_m->getLuas();
        $data["penduduk"] = $this->Admin_m->getPenduduk();
		$this->template->load('template', 'admin/dashboard', $data);
	}

}