<section class="content">
  <div class="box box-primary">

    <!-- content header -->
    <div class="box-header">
        <h2 class="box-title"><big>Edit Akun <?=$row->nama?></big></h2>
        <div class="pull-right">
          <a href="<?=site_url('admin/akun')?>" class="btn btn-warning btn-flat">
            <i class="fa fa-arrow-left"></i>
          </a>
        </div>
    </div>
    <!-- end content header -->

    <!-- isi content -->
    <div class="box-body">
        <div class="row">
        	<div class="col-md-4 ">
        		<form action="" method="post">
        			<div class="form-group <?=form_error('nama') ? 'has-error' : null?>">
        				<label>Nama Kecamatan *</label>
                        <input type="hidden" name="id_akun" value="<?=$row->id_akun?>">
        				<input type="text" name="nama"  value="<?=$this->input->post('nama') ??  $row->nama?>" class="form-control">
        				<?=form_error('nama')?>
        			</div>
        			<div class="form-group <?=form_error('username') ? 'has-error' : null?>">
        				<label>Username *</label>
        				<input type="text" name="username" value="<?=$this->input->post('username') ??  $row->username?>" class="form-control">
        				<?=form_error('username')?>
        			</div>
        			<div class="form-group <?=form_error('password') ? 'has-error' : null?>">
        				<label>Password</label><small> (Biarkan kosong jika tidak ingin mengganti password)</small>
        				<input type="password" name="password" value="<?=$this->input->post('password')?>" class="form-control">
        				<?=form_error('password')?>
        			</div>
        			<div class="form-group <?=form_error('passconf') ? 'has-error' : null?>">
        				<label>Konfirmasi Password</label><small> (Biarkan kosong jika tidak ingin mengganti password)</small>
        				<input type="password" name="passconf" value="<?=$this->input->post('passconf')?>" class="form-control">
        				<?=form_error('passconf')?>
        			</div>
        			<div class="form-group <?=form_error('status') ? 'has-error' : null?>">
        				<label>Status *</label>
        				<select class="form-control" name="status">
                            <?php $status = $this->input->post('status') ? $this->input->post('status') : $row->status ?>
        					<option value="1">Akun Master</option>
        					<option value="2" <?=$status == 2 ? 'selected' : null?>>Akun Kecamatan</option>
        				</select>
        				<?=form_error('status')?>
        			</div>
        			<div class="form-group">
        				<button class="btn btn-success btn-flat" type="submit">
        				    <i class="fa fa-paper-plane"></i> Simpan
	        			</button>
	        			<button class="btn btn-flat" type="reset">Reset</button>
        			</div>
        		</form>
        	</div>
        </div>
    </div>
    <!-- end isi content -->

  </div>
</section>